classdef house
    % Events, characteristics and data of the house
    
    properties
        ID; % identification number
        profile; % power\energy data for each appliance
        categories; % details related to each appliance category 
    end
    
    methods
        
        function obj=house(ID,cat)
            obj.ID = ID;
            for i = 1:length(cat)
                [obj] = set_struct_categories(obj,cat{1,i});
                [obj] = set_struct_profile(obj,cat{1,i});
            end
            [obj] = set_struct_profile(obj,'Total');
        end
        
        function [obj] = set_struct_categories(obj,cat)
            obj.categories.(genvarname(cat)).ID = (genvarname(cat));
            obj.categories.(genvarname(cat)).filename = [];
            obj.categories.(genvarname(cat)).scaling_factor = [];
            obj.categories.(genvarname(cat)).energy_consumption = [];
            obj.categories.(genvarname(cat)).duration = 0;
        end
        
        function [obj] = set_struct_profile(obj,cat)
            obj.profile.(genvarname(cat)).P.data = zeros(1,86400);            
            obj.profile.(genvarname(cat)).Q.data = zeros(1,86400);  
            obj.profile.(genvarname(cat)).P.max = 0;
            obj.profile.(genvarname(cat)).P.min = 0;
            obj.profile.(genvarname(cat)).P.avg = 0;
        end
        
        function [obj] = reset(obj)
                obj.profile.Total.P.data = zeros(1,86400);
                obj.profile.Total.Q.data = zeros(1,86400);
        end
        
        function [obj] = set_cooling_events(obj,appliances,cat,energy)
            obj.profile.(genvarname(cat)).P.data = zeros(1,86400);
            obj.profile.(genvarname(cat)).Q.data = zeros(1,86400);
            daily_energy = energy/365;
            obj.categories.(genvarname(cat)).energy_consumption = daily_energy;
            obj.categories.(genvarname(cat)).duration = 86400;
            appliances_set = fieldnames(appliances.(genvarname(cat))(:));
            len = length(appliances_set);
            file_num = ceil(rand(1,1)*len);
            filename = appliances_set{file_num,1};
            obj.categories.(genvarname(cat)).filename = filename;
            start_val = ceil(rand(1,1)*86400);
            Pdata = [appliances.(genvarname(cat)).(genvarname(filename)).P(start_val:end); appliances.(genvarname(cat)).(genvarname(filename)).P(1:start_val-1)];
            Qdata = [appliances.(genvarname(cat)).(genvarname(filename)).Q(start_val:end); appliances.(genvarname(cat)).(genvarname(filename)).Q(1:start_val-1)];
            appliance_energy = mean(Pdata)*24;
            obj.categories.(genvarname(cat)).scaling_factor = daily_energy/appliance_energy;
            Pdata = Pdata*daily_energy/appliance_energy;
            obj.profile.(genvarname(cat)).P.data = Pdata';
            obj.profile.(genvarname(cat)).Q.data = Qdata';
            obj.profile.(genvarname(cat)).P.max = max(Pdata);
            obj.profile.(genvarname(cat)).P.min = min(Pdata);
            obj.profile.(genvarname(cat)).P.avg = mean(Pdata);
            obj.profile.Total.P.data = obj.profile.Total.P.data + Pdata';
            obj.profile.Total.Q.data = obj.profile.Total.Q.data + Qdata';
        end
        
        function [obj] = set_washing_events(obj,appliances,cat,energy,time_vec)
            obj.profile.(genvarname(cat)).P.data = zeros(1,86400);
            obj.profile.(genvarname(cat)).Q.data = zeros(1,86400);
            daily_energy = energy/365;
            obj.categories.(genvarname(cat)).energy_consumption = daily_energy;
            obj.categories.(genvarname(cat)).scaling_factor = 1;
            appliances_set = fieldnames(appliances.(genvarname(cat))(:));
            len = length(appliances_set);
            file_num = ceil(rand(1,1)*len);
            filename = appliances_set{file_num,1};
            obj.categories.(genvarname(cat)).filename = filename;
            Pdata = appliances.(genvarname(cat)).(genvarname(filename)).P;
            Qdata = appliances.(genvarname(cat)).(genvarname(filename)).Q;
            len = size(Pdata,1);
%             obj.categories.(genvarname(cat)).duration = len;
            appliance_energy = mean(Pdata)*len/3600;
            prob = daily_energy/appliance_energy;
            start_time = 0;
            end_time = 0;
            while prob>0
                i = 1;
                if rand(1,1) < prob
                    check = 1;
                    while check > 0
                        idx = rand(1,1)*length(time_vec);
                        idx2 = floor(idx);
                        if idx2 == 0
                            minute = 0;
                        else
                            minute = time_vec(idx2)*60;
                        end
                        sec = round((idx - floor(idx))*60);
                        time_idx = minute+sec;
                        start_time(i) = time_idx - round(len/2);
                        end_time(i) = start_time(i) + len - 1;
                        if start_time(i) < 1
                            val = abs(start_time(i) - 2);
                            start_time(i) = 1;
                            Pdata2 = Pdata(val:end,:);
                            Qdata2 = Qdata(val:end,:);
                        elseif end_time(i) > 86400
                            end_time(i) = 86400;
                            Pdata2 = Pdata(1:end_time(i)-start_time(i)+1,:);
                            Qdata2 = Qdata(1:end_time(i)-start_time(i)+1,:);
                        else
                            Pdata2 = Pdata;
                            Qdata2 = Qdata;
                        end
                        if i>1
                            for j = 1:i-1
                                log1(j) = start_time(i)<(start_time(j)+len) & start_time(i)>start_time(j);
                                log2(j) = (start_time(i)+len)>start_time(j) & (start_time(i)+len)<(start_time(j)+len);
                            end
                        else
                            log1 = 0;
                            log2 = 0;
                        end
                        check = sum(log1) + sum(log2);
                        obj.profile.(genvarname(cat)).P.data(:,start_time(i):end_time(i)) = Pdata2';
                        obj.profile.(genvarname(cat)).Q.data(:,start_time(i):end_time(i)) = Qdata2';
                        i = i+1;
                    end
                end
                prob = prob-1;
            end
            obj.categories.(genvarname(cat)).duration = sum(end_time - start_time);
            P = obj.profile.(genvarname(cat)).P.data;
            obj.profile.(genvarname(cat)).P.max = max(P);
            obj.profile.(genvarname(cat)).P.min = min(P);
            obj.profile.(genvarname(cat)).P.avg = mean(P);
            obj.profile.Total.P.data = obj.profile.Total.P.data + obj.profile.(genvarname(cat)).P.data;
            obj.profile.Total.Q.data = obj.profile.Total.Q.data + obj.profile.(genvarname(cat)).Q.data;
        end
        
        function [obj] = set_time_varying_events(obj,appliances,cat,energy,time_vec,scale,len_mode,len_max)
            obj.profile.(genvarname(cat)).P.data = zeros(1,86400);
            obj.profile.(genvarname(cat)).Q.data = zeros(1,86400);
            daily_energy = energy/365;
            obj.categories.(genvarname(cat)).energy_consumption = daily_energy;
            obj.categories.(genvarname(cat)).scaling_factor = scale;
            remaining_energy = daily_energy;
            index = 0;
            while remaining_energy > 0
                index = index + 1;
                appliances_set = fieldnames(appliances.(genvarname(cat))(:));
                len = length(appliances_set);
                file_num = ceil(rand(1,1)*len);
                filename = appliances_set{file_num,1};
                obj.categories.(genvarname(cat)).filename = filename;
                Pdata{index,1} = appliances.(genvarname(cat)).(genvarname(filename)).P * scale;
                Qdata{index,1} = appliances.(genvarname(cat)).(genvarname(filename)).Q * scale;
                avg_power = mean(Pdata{index,1});
                device_energy = avg_power*6;
                logic_vect{index,1} = zeros(1,86400);
                remaining_energy = remaining_energy - device_energy;
            end
            var = [];
            n =  index;
            for j = 1:index
                var = [var; j*ones(n,1)];
                n = n-1;
            end
            while daily_energy > 0
                P = zeros(1,86400);
                Q = zeros(1,86400);
                var2 = ceil(rand(1,1)*length(var));
                idx_appliance = var(var2);
                time_length = round(pert_distribution(120, len_mode, len_max, 1));
                idx = rand(1,1)*length(time_vec);
                idx2 = floor(idx);
                if idx2 == 0
                    minute = 0;
                else
                    minute = time_vec(idx2)*60;
                end
                sec = round((idx - floor(idx))*60);
                time_idx = minute+sec;
                start_time = time_idx - round(time_length/2);
                end_time = start_time + time_length - 1;
                if start_time < 1
                    start_time = 1;
                elseif end_time > 86400
                    end_time = 86400;
                end
                time_length = end_time - start_time;
                event_power = mean(Pdata{idx_appliance,1}(1:time_length+1,1));
                event_energy = event_power*time_length/3600;
                if logic_vect{idx_appliance,1}(1,start_time) || logic_vect{idx_appliance,1}(1,end_time)
                    continue
                else
                    check_energy = daily_energy - event_energy;
                    if check_energy < 0
                        if abs(check_energy) < abs(daily_energy)
                            daily_energy = check_energy;
                        else
                            break
                        end
                    else
                        daily_energy = check_energy;
                    end
                    logic_vect{idx_appliance,1}(:,start_time:end_time) = ones(1,time_length+1);
                    P(:,start_time:end_time) = Pdata{idx_appliance,1}(1:time_length+1,1)';
                    Q(:,start_time:end_time) = Qdata{idx_appliance,1}(1:time_length+1,1)';
                    obj.profile.(genvarname(cat)).P.data = obj.profile.(genvarname(cat)).P.data + P;
                    obj.profile.(genvarname(cat)).Q.data = obj.profile.(genvarname(cat)).Q.data + Q;
                end
            end
            final_logic = zeros(1,86400);
            for i = 1:index
                final_logic = final_logic | logic_vect{i,1};
            end
            obj.categories.(genvarname(cat)).duration = sum(final_logic);
            obj.profile.(genvarname(cat)).P.max = max(obj.profile.(genvarname(cat)).P.data);
            obj.profile.(genvarname(cat)).P.min = min(obj.profile.(genvarname(cat)).P.data);
            obj.profile.(genvarname(cat)).P.avg = mean(obj.profile.(genvarname(cat)).P.data);
            obj.profile.Total.P.data = obj.profile.Total.P.data + obj.profile.(genvarname(cat)).P.data;
            obj.profile.Total.Q.data = obj.profile.Total.Q.data + obj.profile.(genvarname(cat)).Q.data;
        end
        
        function [obj] = set_other_events(obj,energy, time_vec)
            obj.profile.Other.P.data = zeros(1,86400);
            obj.profile.Other.Q.data = zeros(1,86400);
            daily_energy = energy/365;
            obj.categories.Other.energy_consumption = daily_energy;
            obj.categories.Other.scaling_factor = 1;
            obj.categories.Other.duration = 86400;
            standby_power = daily_energy/24;
            vect = standby_power*ones(1,86400);
            vect_rand = rand(1,86400)*0.1*standby_power - standby_power/2;
            obj.profile.Other.P.data = vect + vect_rand;
            while daily_energy > 0
                P = zeros(1,86400);
                time_length = round(pert_distribution(30, 600, 3600, 1));
                power_level = rand(1,1)*1.2;
                idx = rand(1,1)*length(time_vec);
                idx2 = floor(idx);
                if idx2 == 0
                    minute = 0;
                else
                    minute = time_vec(idx2)*60;
                end
                sec = round((idx - floor(idx))*60);
                time_idx = minute+sec;
                start_time = time_idx - round(time_length/2);
                end_time = start_time + time_length - 1;
                if start_time < 1
                    start_time = 1;
                elseif end_time > 86400
                    end_time = 86400;
                end
                time_length = end_time - start_time;
                power_vect =  power_level*ones(1,time_length+1) + 0.01*power_level*rand(1,time_length+1);
                event_power = mean(power_vect);
                event_energy = event_power*time_length/3600;
                check_energy = daily_energy - event_energy;
                if check_energy < 0
                    if abs(check_energy) < abs(daily_energy)
                        daily_energy = check_energy;
                    else
                        break
                    end
                else
                    daily_energy = check_energy;
                end
                P(:,start_time:end_time) = power_vect;
                obj.profile.Other.P.data = obj.profile.Other.P.data + P;
            end
            obj.profile.Other.P.max = max(obj.profile.Other.P.data);
            obj.profile.Other.P.min = min(obj.profile.Other.P.data);
            obj.profile.Other.P.avg = mean(obj.profile.Other.P.data);
            obj.profile.Total.P.data = obj.profile.Total.P.data + obj.profile.Other.P.data;
            obj.profile.Total.Q.data = obj.profile.Total.Q.data + obj.profile.Other.Q.data;
        end
                
        function [obj] = calculations(obj)
            P = obj.profile.Total.P.data;
            obj.profile.Total.P.max = max(P);
            obj.profile.Total.P.min = min(P);
            obj.profile.Total.P.avg = mean(P);
            obj.categories.Total.duration = 86400;
        end
        
%         function [obj] = total_calc(obj, unit)
%             obj.profile.Total.P.data = zeros(1,86400);
%             for i=1:length(unit)
%                 obj.profile.Total.P.data = obj.profile.Total.P.data + unit(i).profile.Total.P.data;
%                 obj.profile.Total.Q.data = obj.profile.Total.Q.data + unit(i).profile.Total.Q.data;
%             end
%         end
        
        function [obj] = save_obj(obj)
            save(strcat('houses\',obj.ID))
        end         
        
        function [obj] = remove_obj(obj)
            clear obj
            obj = house('unit_1',{});
        end
            
    end
    
end

