function lic = actxlicense(progid)

if strcmpi(progid, 'CWDSLib.CWDataSocket.1')
lic = 'DM31631964';
return;
end

if strcmpi(progid, 'CWUIControlsLib.CWBoolean.1')
lic = 'mgobggnajebllhogigghmdoocijgbphaplnpkblgeiglndgemabebmhnlpbjmdgg';
return;
end

if strcmpi(progid, 'CWUIControlsLib.CWSlide.1')
lic = 'mfmiomdepanddgoffpmejeeahfdmfkpkoopfofcogjcjehenicjaafepnlnbecgf';
return;
end

if strcmpi(progid, 'CWUIControlsLib.CWNumEdit.1')
lic = 'icmphmnnbaacjfkcjmedjleogjbebkgkmpkhfbfhilommmekimimdndpdlaaobcc';
return;
end

if strcmpi(progid, 'CWUIControlsLib.CWGraph.1')
lic = 'mlfmdlplddfedjolnanlkkdminmnljcnebkkelgdpdamannjllocpbkgbifgengl';
return;
end

if strcmpi(progid, 'CW3DGraphLib.CWGraph3D.1')
lic = 'kpfgobbjdefkidnlmaolnkkbkgilcchgaanpphmebbjnmoliikaandlppknbogjiehhpap';
return;
end

if strcmpi(progid, 'CWUIControlsLib.CWKnob.1')
lic = 'mpmipnbelbfbddopbpeejffcfbhenkolmmlbgncpglcnepenjclaffopjkfdehgp';
return;
end

if strcmpi(progid, 'AIR.AirCtrl.1')
lic = 'Copyright (c) 1996 ';
return;
end

if strcmpi(progid, 'MSComctlLib.ImageComboCtl.2')
lic = '9368265E-85FE-11d1-8BE3-0000F8754DA1';
return;
end

if strcmpi(progid, 'MSComctlLib.ImageListCtrl.2')
lic = '9368265E-85FE-11d1-8BE3-0000F8754DA1';
return;
end

if strcmpi(progid, 'InfoPath.Editor.3')
lic = 'InfoPath.Editor';
return;
end
