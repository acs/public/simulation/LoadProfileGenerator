function samples = pert_distribution(min_value, mode, max_value, n)

mean_value = (min_value + max_value + 4*mode)/6;
if mean_value == mode
   var = (max_value - min_value)/1000;
   mode = mode - var;
   mean_value = (min_value + max_value + 4*mode)/6;
end  
   
v = ((mean_value - min_value)*(2*mode - min_value - max_value))/ ((mode - mean_value)*(max_value - min_value));
w = v*(max_value - mean_value)/(mean_value - min_value);

samples = betarnd(v,w,n,1)*max_value;
