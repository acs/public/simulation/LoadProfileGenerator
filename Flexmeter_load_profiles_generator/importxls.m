%% Import function executes different code, when changing third input parameter 'varargin{3}'
function [ varargout ] = importxls(varargin)

    % switch-case instruction based on input parameters
    switch varargin{2}
        case 1
            % no namestring given
            [numbers, strings] = xlsread(varargin{1}, varargin{3});
            varargout{1} = numbers;
        case 2
            % regular standard event
            [numbers, strings] = xlsread(varargin{1}, varargin{3});
            varargout{1} = numbers;
            varargout{2} = strings;
        case 3
            [version, sheetnames] = xlsfinfo(varargin{1});
            for i=1:length(sheetnames)
                [numbers, strings] = xlsread(varargin{1}, sheetnames{i});
                varargout{1}.std_list(i).catname = sheetnames{i};
                %varargout{1}.std_list(i).num = numbers;
                varargout{1}.std_list(i).num.max = max(numbers);
                varargout{1}.std_list(i).num.min = min(numbers);
                varargout{1}.std_list(i).str = strings;              
            end
            varargout{1}.catnames = sheetnames;
        case 4
            [numbers, strings] = xlsread(varargin{1}, varargin{3});
            % special event import routine
            runvar=1;
            for i=1:length(numbers)-1
                dat1=datevec(strings(i,1), 'dd/mm/yyyy HH:MM:SS');
                dat2=datevec(strings(i+1,1), 'dd/mm/yyyy HH:MM:SS');
                dat=dat2-dat1;
                secsum = dat(1)*60*60*24*365 + dat(2)*60*60*24*365/12 + dat(3)*60*60*24 + dat(4)*60*60 + dat(5)*60 + dat(6);
                temp_profile(runvar:runvar+secsum-1) = numbers(i);
                runvar=runvar+secsum;
            end
            % output parameters
            varargout{1}.num = temp_profile; %time variant behavior
            varargout{1}.str = varargin{2}; %name
            varargout{1}.len = length(temp_profile); %length
        otherwise
            % error handler
            if isempty(numbers)
                numbers = 0;
            end
            if isempty(strings)
                strings = 'null';
            end
            varargout{1} = numbers;
            varargout{2} = strings;
    end
end
