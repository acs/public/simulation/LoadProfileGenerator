%% Step0 - GUI sensetive functions
% Initializing GUI, precalculating variables
function varargout = generator(varargin)
% GENERATOR MATLAB code for generator.fig
% See also: GUIDE, GUIDATA, GUIHANDLES

% initialization code - DO NOT EDIT --->
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @generator_OpeningFcn, ...
                   'gui_OutputFcn',  @generator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end % <---

% Executes just before generator is made visible, first(example) house is calculated.
function generator_OpeningFcn(hObject, eventdata, handles, varargin)

%import time-vector for export-fct (massive calculation time)
%and address vector for EXCEL columns 
handles.GUI = load('sources\excel_export.mat');

%loading VDEW and other data

handles.DATA.SLP.data = importxls('sources\loadprofile_VDEW.xls', 1, 'H0');

[categories.num, categories.str] = importxls('sources\categories.xls',2 ,'categories');

handles.DATA.categories.prob  = categories.num(:,1);
handles.DATA.categories.avg = categories.num(:,2);
handles.DATA.categories.str = categories.str;

handles.GUI.categories.num = handles.DATA.categories.prob;
handles.GUI.categories.str = handles.DATA.categories.str;


handles.DATA.users = importxls('sources\userdata.xls', 3);

% special user import (returns struct with data...)
handles.DATA.users.spec_users.dishwasher = importxls('sources\dishwasher.xlsx', 4, 'dishwasher');
handles.DATA.users.spec_users.washing = importxls('sources\washingmachine.xlsx', 4, 'washingmachine');

% stores data to special event-class
handles.DATA.users.spec_users.e_stor(1) = event(01);
handles.DATA.users.spec_users.e_stor(2) = event(02);
handles.DATA.users.spec_users.e_stor(3) = event(03);
handles.DATA.users.spec_users.e_stor(4) = event(04);
handles.DATA.users.spec_users.e_stor(5) = event(05);
handles.DATA.users.spec_users.e_stor(6) = event(06);
handles.DATA.users.spec_users.e_stor(7) = event(07);
handles.DATA.users.spec_users.e_stor(8) = event(08, handles.DATA.users.spec_users.dishwasher.str, handles.DATA.users.spec_users.dishwasher.num, length(handles.DATA.users.spec_users.dishwasher.num));
setpushbuttonvisible(hObject, handles, 8, 1);
handles.DATA.users.spec_users.e_stor(9) = event(09, handles.DATA.users.spec_users.washing.str, handles.DATA.users.spec_users.washing.num, length(handles.DATA.users.spec_users.washing.num));
setpushbuttonvisible(hObject, handles, 9, 1);

% category profile definition
% cooling(cat 1) & timerange
cooling_profile_dur=15;
cooling_profile=zeros(60*cooling_profile_dur,1);
cooling_profile_len=length(cooling_profile);
cooling_profile(1:cooling_profile_len*2/cooling_profile_dur)=1;
cooling_profile(cooling_profile_len*2/cooling_profile_dur+1:cooling_profile_len)=.75;

% constant distribution (2/15: 1; 13/15: 0.75)
handles.DATA.users.std_list(1).distribution = cooling_profile;
% constant duration (15 minutes)
handles.DATA.users.std_list(1).duration = [cooling_profile_dur*60, cooling_profile_dur*60];

% PC/Comm(cat 2) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(2).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(2).duration = [5*60, 60*60];

% Audio/TV(cat 3) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(3).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(3).duration = [5*60, 60*60];

% Warm_water(cat 4) & timerange
% constant distribution (rectangle profile) [Conn Power high]
handles.DATA.users.std_list(4).distribution = 0.5;
% minimum duration: 0.5min ; maximum duration: 10min
handles.DATA.users.std_list(4).duration = [0.5*60, 10*60];

% light(cat 5) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(5).distribution = 1;
% minimum duration: 1min ; maximum duration: 10min
handles.DATA.users.std_list(5).duration = [1*60, 10*60];

% cooking(cat 6) & timerange 
cooking_profile = importxls('sources\cooking.xlsx', 1, 'cooking');
cooking_profile(:,1) = [];

% constant distribution (rectangle profile)
handles.DATA.users.std_list(6).distribution = cooking_profile;
% constant duration: 34min
handles.DATA.users.std_list(6).duration = [10*60, 60*60];
% electrical oven is only partionally used (20% - one plate)
handles.DATA.users.std_list(6).num.max = 1/5*handles.DATA.users.std_list(6).num.max;
handles.DATA.users.std_list(6).num.min = 1/5*handles.DATA.users.std_list(6).num.min;


% drying(cat 7) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(7).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(7).duration = [30*60, 60*60];

% dishwasher(cat 8) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(8).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(8).duration = [30*60, 60*60];

% washing(cat 9) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(9).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(9).duration = [30*60, 60*60];

% standby(cat 10) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(10).distribution = 1;
% standy constant over the day
handles.DATA.users.std_list(10).duration = [24*60*60, 24*60*60];




% GUI/DATA: Standard load profile and timescale
% set timescale to standard value 60 sec
handles.DATA.GUIdata.scale = 60*60*24;
handles.DATA.GUIdata.scale_input = 60;
handles.DATA.GUIdata.scale_used = 24/(handles.DATA.GUIdata.scale_input/(60*60));

% used column
handles.DATA.SLP.cur_col=11;

% initial generation of events
handles.DATA.SLP.size = size(handles.DATA.SLP.data);
handles.DATA.SLP.sum = zeros(1,handles.DATA.SLP.size(2)-1);
handles.DATA.SLP.prob = zeros(handles.DATA.SLP.size(1), handles.DATA.SLP.size(2)-1);
handles.DATA.SLP.prob_normed = zeros(handles.DATA.SLP.size(1), handles.DATA.SLP.size(2)-1);

% edit load profile to relative probability
for j=2:handles.DATA.SLP.size(2)
handles.DATA.SLP.cur_max=eps;
    for i=1:handles.DATA.SLP.size(1)
        handles.DATA.SLP.sum(j-1) = handles.DATA.SLP.sum(j-1)+handles.DATA.SLP.data(i,j);
        
        if(handles.DATA.SLP.data(i,j)>handles.DATA.SLP.cur_max)
            handles.DATA.SLP.cur_max=handles.DATA.SLP.data(i,j);
        end        
    end
    for i=1:handles.DATA.SLP.size(1)
        handles.DATA.SLP.prob(i,j-1) = handles.DATA.SLP.data(i,j)/handles.DATA.SLP.sum(j-1);
        %norming to one, not used
        handles.DATA.SLP.prob_normed(i,j-1) = handles.DATA.SLP.prob(i,j-1)/(handles.DATA.SLP.cur_max/handles.DATA.SLP.sum(j-1));
    end
end

% set initial load
handles.DATA.mSLP.data = handles.DATA.SLP.prob(:,handles.DATA.SLP.cur_col-1);
handles.DATA.GUIdata.usage_data = 1000000;

% stores load to special variable
handles.DATA.GUIdata.categories.num=handles.DATA.categories.prob;



% GUI-textfield refreshing: length of special events
handles.GUI.correction_factor = get(handles.corr_factor_checkbox, 'Value');
for l=1:length(handles.DATA.users.spec_users.e_stor)
    len = length(handles.DATA.users.spec_users.e_stor(l).profile.data);
    set(handles.(genvarname(['cat' num2str(l) '_ltext'])), 'string', sec2timestr(len));
end

temp_sum = 0;
len=length(handles.DATA.categories.str);

for i=1:len
    % get used categories from checkboxes
    handles.GUI.categories.checkbox(i) = get(handles.(genvarname(['cat' num2str(i) '_checkbox'])), 'Value');
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end

for j=1:length(handles.DATA.categories.prob)
    if(handles.GUI.categories.checkbox(j))
        handles.DATA.GUIdata.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
    else
        handles.DATA.GUIdata.categories.num(j) = 0;
    end
end

% update category load text fields
for k=1:len
    set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.GUIdata.categories.num(k)*100, '%7.2f'), ' %']);
    set(handles.(genvarname(['cat' num2str(k) '_static_text'])), 'string', ['(' num2str(handles.DATA.categories.prob(k)*100, '%7.2f'), '%)']);
    set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k), '%7.1f'), 'Wh']);
end

% pre-calculates vdew probability distribution
handles = premeasure_vdewdistribution(handles);

handles.DATA.simulation_period(1).date = floor(datenum(now));
handles.DATA.simulation_period(1).profile_num = SLP_number(handles.DATA.simulation_period(1).date);
handles.DATA.simulation_period(1).energy = handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(datestr(floor(datenum(now)),'yyyy'))))*(1 + handles.GUI.correction_factor*(vdew_factor(floor(datenum(now)))-1));
% DATA:
% create a first house with std data
handles.first = house(0, handles.DATA.GUIdata, handles.DATA.simulation_period, handles.DATA.users);

handles.first = handles.first.standby_power_event(1);
handles.first = handles.first.cooling_events(1);
handles.first = handles.first.continuous_events(1);
%handles.first = handles.first.spec_events(); no measured devices
handles.first = handles.first.insert_events(vdew_rand(handles.DATA.vdew(10), handles.DATA.GUIdata.scale_input, (handles.first.event_count()+20)), 1);

% GUI: plot first results

% plot output profile with the standard house data
bar(handles.output_axes, handles.first.days(1).loadcurve);
bar(handles.single_output_axes, handles.first.days(1).loadcurve);

% modify plots for right axes assignment
mod_plot(handles.output_axes, length(handles.first.days(1).loadcurve), 'load[W]');
mod_plot(handles.single_output_axes, length(handles.first.days(1).loadcurve), 'load[W]');

% set energy text field to standard value
set(handles.norm_load_text, 'string', [num2str((sum(handles.first.days(1).loadcurve)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
set(handles.sing_load_text, 'string', [num2str((sum(handles.first.days(1).loadcurve)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);

% Choose default command line output for generator
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = generator_OutputFcn(hObject, eventdata, handles) 
% no interaction with the command line, except error handling
varargout{1} = handles.output;

%% Step1 - VDEW selection/power demand selection functions
% Input: yearly consumption
function yearly_load_edit_Callback(hObject, eventdata, handles)
cur_load = str2double(get(handles.yearly_load_edit,'String'));
if(isnan(cur_load))
    cur_load = 1000;
    set(handles.yearly_load_text, 'string', 'NaN, used 1000 kWh(std)');
else
    set(handles.yearly_load_text, 'string', [num2str(cur_load), ' kWh']);
end
handles.DATA.GUIdata.usage_data = 1000*cur_load;
set(handles.calc_cat_text, 'string', 'parameter changed');

guidata(hObject,handles);

% Popupmenu selection of used timescale
function timescale_popupmenu_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
switch str{val}
    case '1 sec'
        handles.DATA.GUIdata.scale_input = 1;
    case '2 sec'
        handles.DATA.GUIdata.scale_input = 2;
    case '5 sec'
        handles.DATA.GUIdata.scale_input = 5;
    case '10 sec'
        handles.DATA.GUIdata.scale_input = 10;
    case '15 sec'
        handles.DATA.GUIdata.scale_input = 15;
    case '20 sec'
        handles.DATA.GUIdata.scale_input = 20;
    case '30 sec'
        handles.DATA.GUIdata.scale_input = 30;
    case '60 sec'
        handles.DATA.GUIdata.scale_input = 60;
end

handles.DATA.GUIdata.scale_used = 24/(handles.DATA.GUIdata.scale_input/(60*60));

% Update handles structure
guidata(hObject,handles);

%% Step2 - Category selection and calculation
% execute after applying all inputs for category settings by users command
function calc_cat_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles);

temp_sum = 0;

for i=1:length(handles.GUI.categories.checkbox)
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end

if(temp_sum == 0)
    set(handles.calc_cat_text, 'string', '0 - failed');
else
    for j=1:length(handles.DATA.categories.prob)
        if(handles.GUI.categories.checkbox(j))
            handles.DATA.GUIdata.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
        else
            handles.DATA.GUIdata.categories.num(j) = 0;
        end
    end
    
    handles = premeasure_vdewdistribution(handles);

    % cat load text-field update
    for k=1:length(handles.DATA.categories.prob)
        if(strcmp(get(handles.(genvarname(['cat' num2str(k) '_axes'])), 'Visible'), 'on'))
            handles.DATA.users.spec_users.e_stor(k).to_use=1;
            % cycle refresh
            suma=sum(handles.DATA.users.spec_users.e_stor(k).profile.data)/3600;
            usage_factor=(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k))/suma;
            set(handles.(genvarname(['cat' num2str(k) '_consumtext'])), 'String', [num2str(suma/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
        end
        set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k), '%7.1f'), 'Wh']);
        set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.GUIdata.categories.num(k)*100, '%7.2f'), ' %']);
    end

    set(handles.calc_cat_text, 'string', 'calc done');
    
end
busy_switch(handles);
guidata(hObject,handles);

function handles = premeasure_vdewdistribution(handles)
% overwrites used data for event spreading
handles.DATA.mSLP.data = handles.DATA.SLP.prob-(handles.DATA.GUIdata.categories.num(1)+handles.DATA.GUIdata.categories.num(10))/96;
% if prob<0 --> prob=0
handles.DATA.mSLP.data(find(handles.DATA.mSLP.data<0))=0;

vdew_reduced_sum=sum(handles.DATA.mSLP.data);
for i=1:length(vdew_reduced_sum)
handles.DATA.mSLP.data(:,i)=handles.DATA.mSLP.data(:,i)/vdew_reduced_sum(i);
end

len=10000;
for j=1:length(vdew_reduced_sum)
    handles.DATA.vdew(j).interval=zeros(len,1);
    handles.DATA.vdew(j).rescaled=round(handles.DATA.mSLP.data(:,j)*len);
    handles.DATA.vdew(j).startpos=zeros(length(handles.DATA.vdew(j).rescaled),1);
end
% gen...
for j=1:length(vdew_reduced_sum)
    handles.DATA.vdew(j).startpos(1)=1;
    handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(1):handles.DATA.vdew(j).rescaled(1))=1;
    for i=2:length(handles.DATA.mSLP.data)
       handles.DATA.vdew(j).startpos(i)=handles.DATA.vdew(j).startpos(i-1)+handles.DATA.vdew(j).rescaled(i-1);
       handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(i):(handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i)))=i;
    end
    % error catching
    if((handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i))<length(handles.DATA.vdew(j).interval))
        handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i)+1:length(handles.DATA.vdew(j).interval))=i;
    end
end
% returns a random number based on vdew propability distribution
function startpos_list = vdew_rand(vdew, scale_input, count)
startpos_list=zeros(count,1);
for i=1:count
    pos_y = floor(rand*length(vdew.interval))+1;
    pos_x = vdew.interval(pos_y);
    frac = (pos_y-vdew.startpos(pos_x))/vdew.rescaled(pos_x);
    startpos_list(i) = floor((pos_x-1+frac)*15*60/scale_input)+1;
end


%% Step3 - Main function for simulation 
% Executes loop for #houses
function plot_complete_pushbutton_Callback(hObject, eventdata, handles)

    busy_switch(handles);
    handles.no_houses = str2double(get(handles.input_houses_edit,'String'));
    handles.DATA.complete_loadcurve = zeros(length(handles.DATA.simulation_period), handles.DATA.GUIdata.scale_used);
    %hide multiple day selection
    set(handles.day_num_popup, 'Visible', 'off');
    set(handles.single_datum_text, 'Visible', 'off');
    set(handles.day_num_norm_popup, 'Visible', 'off');
    set(handles.norm_datum_text, 'Visible', 'off');
    %error-catching
    if(isnan(handles.no_houses))
        handles.no_houses = 1;
    end
    
    set(handles.stop_pushbutton, 'Visible', 'on');
    set(handles.stop_text, 'Visible', 'off');
    
    h_run_var = 0;
    handles.stopprocess = 0;
    
    for i=1:handles.no_houses
        handles.stor(i) = house(i, handles.DATA.GUIdata, handles.DATA.simulation_period, handles.DATA.users);
    end
tic % time counter start tag
for j=1:length(handles.DATA.simulation_period)
    for i = 1:handles.no_houses
        if(strcmp(get(handles.stop_text, 'String'), 'STOP'))
            break;
        end
        h_run_var = h_run_var + 1;

        handles.stor(i) = handles.stor(i).standby_power_event(j);
        if(handles.GUI.categories.checkbox(1))
            handles.stor(i) = handles.stor(i).cooling_events(j);
        end
        handles.stor(i) = handles.stor(i).spec_events(j);
        handles.stor(i) = handles.stor(i).continuous_events(j);
        handles.stor(i) = handles.stor(i).insert_events(vdew_rand(handles.DATA.vdew(handles.DATA.simulation_period(j).profile_num), handles.DATA.GUIdata.scale_input, handles.stor(i).event_count()+40), j);
        if(length(handles.DATA.simulation_period)>1)
            handles.stor(i) = handles.stor(i).next_day(j);
            
        end
        % generate complete load curve
        handles.DATA.complete_loadcurve(j,:)=handles.DATA.complete_loadcurve(j,:)+transpose(handles.stor(i).days(j).loadcurve);

        % progressbar update
        %uiprogressbar(i, handles.no_houses, handles);
        uiprogressbar(j, length(handles.DATA.simulation_period), handles);
    end
end
    if(h_run_var==handles.no_houses)
        set(handles.stop_text, 'string', 'finished');
    else
        set(handles.stop_text, 'string', 'stopped');
    end
    
    set(handles.stop_pushbutton, 'Visible', 'off');
    set(handles.stop_text, 'Visible', 'on');
    
    if (h_run_var == 0)
        h_run_var = 1;
    end
    handles.DATA.complete_loadcurve = handles.DATA.complete_loadcurve/length(handles.DATA.simulation_period);    
        
    set(handles.xls_export1_edit, 'String', num2str(1));
    set(handles.xls_export2_edit, 'String', num2str(length(handles.stor)));

    bar(handles.output_axes, handles.DATA.complete_loadcurve(1,:));
    mod_plot(handles.output_axes, length(handles.DATA.complete_loadcurve(1,:)), 'load[W]');
    
    % text update
    set(handles.calc_cat_text, 'string', '');

    set(handles.no_houses_text, 'string', ['ploted for ', num2str(h_run_var), ' houses']);
    set(handles.norm_load_text, 'string', [num2str((sum(handles.DATA.complete_loadcurve(1,:))/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
    
    if(j>1)
        % single plot refresh, multiple days
        set(handles.day_num_popup, 'Visible', 'on');
        set(handles.single_datum_text, 'Visible', 'on');
        set(handles.day_num_norm_popup, 'Visible', 'on');
        set(handles.norm_datum_text, 'Visible', 'on');
        for m=1:j
            no_str1(m).num = num2str(m);
        end
        ce_str1 = struct2cell(no_str1);
        set(handles.day_num_popup, 'String', ce_str1);
        set(handles.day_num_norm_popup, 'String', ce_str1);
    end
    
    % single plot refresh, multiple households
    for n=1:length(handles.stor)
        no_str(n).num = num2str(n);
        %no_str(n).num = ['House No. ',num2str(n)];
    end
    ce_str = struct2cell(no_str);
    set(handles.houseno_popup, 'String', ce_str);
    
    bar(handles.single_output_axes, handles.stor(1).days(1).loadcurve);
    handles = mod_single_plot(handles, 1, 1);
    
busy_switch(handles);

guidata(hObject,handles);

% Callback for stop button, dialog for confim
function stop_pushbutton_Callback(hObject, eventdata, handles)
selection = questdlg('Do you want to stop this process?',...
                     'Stop process',...
                     'Yes','No','Yes');
switch selection,
   case 'Yes',
         set(handles.stop_text, 'String', 'STOP');
         drawnow;
   case 'No'
         return
end


%% Step4 - functions to display & save results
% Callback function for single plot update by dropdownmenu selection
function houseno_popup_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');

pos = str2double(str{val});
bar(handles.single_output_axes, handles.stor(pos).days(1).loadcurve);
if(isfield(handles, 'stor')) 
    handles = mod_single_plot(handles, pos, 1);
end
guidata(hObject,handles);

% helpfunction for modification of single household plot
function handles = mod_single_plot(handles, pos, day)
runvar=1;
hold on;
for m=2:length(handles.stor(pos).spec_estor)
    if(handles.stor(pos).spec_estor(m).profile.cycles(day)>0)
        for i=1:length(handles.stor(pos).spec_estor(m).startpos(day).num)
            rndclr=[rand, rand, rand];
            if(handles.stor(pos).spec_estor(m).startpos(day).num(i)+handles.stor(pos).spec_estor(m).profile.duration>handles.stor(pos).GUIdata.scale_used)
                plot([handles.stor(pos).spec_estor(m).profile.data(handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).startpos(day).num(i)+1:handles.stor(pos).spec_estor(m).profile.duration); zeros((handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).profile.duration), 1); handles.stor(pos).spec_estor(m).profile.data(1:handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).startpos(day).num(i))], 'color', rndclr);
            else
                plot([zeros(handles.stor(pos).spec_estor(m).startpos(day).num(i), 1); handles.stor(pos).spec_estor(m).profile.data; zeros(handles.stor(pos).GUIdata.scale_used-(handles.stor(pos).spec_estor(m).startpos(day).num(i)+handles.stor(pos).spec_estor(m).profile.duration),1)], 'color', rndclr);
            end
            runvar=runvar+1;
        end
    end
end
hold off;

mod_plot(handles.single_output_axes, length(handles.stor(pos).days(day).loadcurve), 'load[W]');
set(handles.single_datum_text, 'string', datestr(handles.stor(pos).days(day).date, 'yyyy-mm-dd'));
set(handles.houseno_text, 'string', ['House ', num2str(pos), ' plotted']);
set(handles.sing_load_text, 'string', [num2str((sum(handles.stor(pos).days(day).loadcurve)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);



% saves handles structure to .mat file
function save_data_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('handles_v','Save your GUI data');

if pathname == 0 %if the user pressed cancelled, then exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname, filename); 
busy_switch(handles);
%FileName = fullfile(saveDataName, 'Handles.mat');
save(saveDataName, 'handles'); 
busy_switch(handles);

% saves a calculation to a .fig file
function save_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('guidata_v','Save your GUI settings');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname,filename); 
busy_switch(handles);
%saves the gui data
hgsave(saveDataName);
busy_switch(handles);

% loads a saved calculation and closes the old
function load_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to choose which settings to load
[filename, pathname] = uigetfile('*.fig', 'Choose the GUI settings file to load');

%construct the path name of the file to be loaded
loadDataName = fullfile(pathname,filename);

%this is the gui that will be closed once we load the new settings
theCurrentGUI = gcf;  

%load the settings, which creates a new gui
hgload(loadDataName); 

%closes the old gui
close(theCurrentGUI);

% export function XLS
function xls_export_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the ,file
[filename,pathname] = uiputfile('*.xlsx','Save your House Data to xls');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname,filename); 

busy_switch(handles);

len = length(handles.complete_load);
xlswrite(saveDataName, handles.GUI.time(86400/len).str, 'comp_load', ['A1:A' num2str(len)]);
xlswrite(saveDataName, handles.complete_load, 'comp_load', ['B1:B' num2str(len)]);

startp = str2double(get(handles.xls_export1_edit,'String'));
endp = str2double(get(handles.xls_export2_edit,'String'));

addr_len = length(handles.GUI.excel_export);
if(isfield(handles, 'stor'))
    runvar=startp;
    for i=1:floor(endp/addr_len)
        for col=1:addr_len
        xlswrite(saveDataName, handles.stor(runvar).profile.loadcurve, ['houses' num2str(i)], [char(handles.GUI.excel_export(col)) '1:' char(handles.GUI.excel_export(col)) num2str(len)]);
        runvar=runvar+1;
        end
    end
    
    for col=1:mod(endp, addr_len) 
        xlswrite(saveDataName, handles.stor(runvar).profile.loadcurve, ['houses' num2str(i+1)], [char(handles.GUI.excel_export(col)) '1:' char(handles.GUI.excel_export(col)) num2str(len)]);
        runvar=runvar+1;
    end
else
    xlswrite(saveDataName, handles.first.profile.loadcurve, ['house_' 'first']);
end
busy_switch(handles);

% time string generation - excessive time need - atm: not in use
function outp = temp(scale_input)
outp{1,1} = datestr([2011,1,1,0,0,0], 'HH:MM:SS');
for i=2:scale_input
    %datestr(datenum(datestr([2009,1,1,11,7,18], 'HH:MM:SS'))+1/(24*3600)*86400/scale_input, 'HH:MM:SS')
    
    tmp = datenum(outp{i-1,1});  
    outp{i,1} = datestr(tmp+1/scale_input, 'HH:MM:SS');
end


%% helpfunctions for GUI apperance

% function modifies a plot axes based on input parameters
function mod_plot(hand, leng, ystr)
% x-axis
set(hand,'XLim', [0 leng]);
set(hand,'XTick', [0:leng/24:leng]);
set(hand,'XTickLabel',[' 0';'  ';' 2';'  ';' 4';'  ';' 6';'  ';' 8';'  ';'10';'  ';'12';'  ';'14';'  ';'16';'  ';'18';'  ';'20';'  ';'22';'  ';'24']);
set(get(hand, 'XLabel'), 'String' ,'time[h]');
set(hand,'XGrid','on');
% y-axis
set(get(hand, 'YLabel'), 'String' ,ystr);
set(hand,'yGrid','on');

% changes visibility for categories / special event pushbuttons
function cat1_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 1, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat2_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 2, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat3_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 3, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat4_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 4, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat5_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 5, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat6_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 6, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat7_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 7, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat8_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 8, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat9_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 9, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat10_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 10, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function setpushbuttonvisible(hObject, handles, nr, vis)
handles.GUI.categories.checkbox(nr) = get(handles.(genvarname(['cat' num2str(nr) '_checkbox'])), 'Value');
if(vis)
    status = get(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible');
    status2 = get(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible');
    if(strcmp(status, 'on') && strcmp(status2, 'off'))
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    elseif(strcmp(status2, 'on') && ~strcmp(status, 'on'))
        xbuttonupdate(hObject, handles, nr);
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    else
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');    
    end
end
guidata(hObject,handles);

% updates textfields and applies appearance of axes for special-events
function cat1_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 1);

function cat2_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 2);

function cat3_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 3);

function cat4_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 4);

function cat5_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 5);

function cat6_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 6);

function cat7_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 7);

function cat8_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 8);

function cat9_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 9);

function cat10_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 10);

function mod_spec_load(handles, num)
set(handles.(genvarname(['cat' num2str(num) '_spec_pushbutton'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(num) '_axes'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_x'])), 'Visible', 'on');
bar(handles.(genvarname(['cat' num2str(num) '_axes'])), handles.DATA.users.spec_users.e_stor(num).profile.data);

handles.DATA.users.spec_users.e_stor(num).to_use=1;

leng=length(handles.DATA.users.spec_users.e_stor(num).profile.data);
maxi=max(handles.DATA.users.spec_users.e_stor(num).profile.data);
suma=sum(handles.DATA.users.spec_users.e_stor(num).profile.data)/3600;
usage_factor=(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(num))/suma;
set(handles.(genvarname(['cat' num2str(num) '_ltext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XLim', [0 leng]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTick', 0:leng:leng);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YLim', [0 (maxi*1.1)]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTick', 0:maxi:maxi);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'String', [num2str(suma/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
set(handles.calc_cat_text, 'string', 'parameter changed');

% updates the visibility of the special event use buttons
function cat1_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 1);

function cat2_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 2);

function cat3_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 3);

function cat4_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 4);

function cat5_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 5);

function cat6_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 6);

function cat7_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 7);

function cat8_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 8);

function cat9_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 9);

function cat10_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 10);

function xbuttonupdate(hObject, handles, nr)

set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_x'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_ltext'])), 'Visible', 'off');
cla(handles.(genvarname(['cat' num2str(nr) '_axes']))); %clears axis-objects
set(handles.(genvarname(['cat' num2str(nr) '_consumtext'])), 'Visible', 'off');
handles.DATA.users.spec_users(nr).to_use=0;

set(handles.calc_cat_text, 'string', 'parameter changed');
guidata(hObject,handles);

% guibusy text visibility
function busy_switch(handles)
status = get(handles.guibusy_text, 'Visible');
if(strcmp(status,'off')) 
    set(gcf, 'Pointer', 'watch');
    set(handles.guibusy_text, 'Visible', 'on');
else
    set(gcf, 'Pointer', 'arrow');
    set(handles.guibusy_text, 'Visible', 'off');
end
drawnow;

% progressbar update function
function uiprogressbar(x, z, handles)
frac = x/z;
pos = get(handles.progressbar_text,'position');
pos2 = get(handles.progress_text, 'position');
% bar position & length calculation
pos(3) = frac*(pos2(3)-2);
set(handles.progressbar_text,'position',pos,'string',sprintf('%.0f%%',frac*100))
outp = sec2timestr(fix(toc*(z-x)/x));
set(handles.progressstatus_text, 'string', [outp, ' remaining']);
drawnow

% this function returns string for time left - input: time left in seconds
function timestr = sec2timestr(sec)
% Convert a time measurement from seconds into a human readable string.

% Convert seconds to other units
w = floor(sec/604800); % Weeks
sec = sec - w*604800;
d = floor(sec/86400); % Days
sec = sec - d*86400;
h = floor(sec/3600); % Hours
sec = sec - h*3600;
m = floor(sec/60); % Minutes
sec = sec - m*60;
s = floor(sec); % Seconds

% Create time string
if w > 0
    if w > 9
        timestr = sprintf('%d week', w);
    else
        timestr = sprintf('%d week, %d day', w, d);
    end
elseif d > 0
    if d > 9
        timestr = sprintf('%d day', d);
    else
        timestr = sprintf('%d day, %d hr', d, h);
    end
elseif h > 0
    if h > 9
        timestr = sprintf('%d hr', h);
    else
        timestr = sprintf('%d hr, %d min', h, m);
    end
elseif m > 0
    if m > 9
        timestr = sprintf('%d min', m);
    else
        timestr = sprintf('%d min, %d sec', m, s);
    end
else
    timestr = sprintf('%d sec', s);
end



%% All 'unused' callback functions down here (EDIT Boxes)

% Executes during object creation, after setting all properties. All create functions down here
% popup menues 
function select_data_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function houseno_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function timescale_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% unused edit box create functions
function yearly_load_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function input_houses_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export1_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export2_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function from_year_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function from_month_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function from_day_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function to_year_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function to_month_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function to_day_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% unused callbacks
function input_houses_edit_Callback(hObject, eventdata, handles)
function xls_export1_edit_Callback(hObject, eventdata, handles)
function xls_export2_edit_Callback(hObject, eventdata, handles)

function from_year_edit_Callback(hObject, eventdata, handles)
function from_month_edit_Callback(hObject, eventdata, handles)
function from_day_edit_Callback(hObject, eventdata, handles)

function to_year_edit_Callback(hObject, eventdata, handles)
function to_month_edit_Callback(hObject, eventdata, handles)
function to_day_edit_Callback(hObject, eventdata, handles)


function SLP_selection_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles);

tmp_from(1) = str2double(get(handles.from_year_edit,'String'));
tmp_from(2) = str2double(get(handles.from_month_edit,'String'));
tmp_from(3) = str2double(get(handles.from_day_edit,'String'));
tmp_from_datenum = datenum(tmp_from);

tmp_to(1) = str2double(get(handles.to_year_edit,'String'));
tmp_to(2) = str2double(get(handles.to_month_edit,'String'));
tmp_to(3) = str2double(get(handles.to_day_edit,'String'));
tmp_to_datenum = datenum(tmp_to);

runvar=1;
for i=tmp_from_datenum:tmp_to_datenum
    handles.DATA.simulation_period(runvar).profile_num = SLP_number(tmp_from_datenum+runvar-1);
    handles.DATA.simulation_period(runvar).date = tmp_from_datenum+runvar-1;
    handles.DATA.simulation_period(runvar).energy = handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(datestr(tmp_from_datenum+runvar-1,'yyyy'))))*(1 + handles.GUI.correction_factor*(vdew_factor(tmp_from_datenum+runvar-1)-1));
    runvar=runvar+1;
end
busy_switch(handles);
guidata(hObject,handles);


function profile_number=SLP_number(day_vec)
%1. season selection; season=[sunday workday saturday]
day_vec = datevec(day_vec);
winter = [3 4 2];
interim = [9 10 8];
summer = [6 7 5];

switch day_vec(2)
    case {1,2,11,12} %winter
        tmp_number = winter;
    case 3
        if(day_vec(3)<=20) %winter
            tmp_number = winter;
        else %interim
            tmp_number = interim;
        end
    case {4,10} %interim
        tmp_number = interim;
    case 5
        if(day_vec(3)<=14) %interim
            tmp_number = interim;
        else %summer
            tmp_number = summer;
        end
    case {6,7,8} %summer
        tmp_number = summer;
    case 9
        if(day_vec(3)<=14) %summer
            tmp_number = summer;
        else %interim
            tmp_number = interim;
        end
end

%2. holiday check
%2.1 easter calculation
a = mod(day_vec(1),19);
b = mod(day_vec(1),4);
c = mod(day_vec(1),7);
d = mod((19*a + 24), 30);
e = mod((2*b + 4*c + 6*d + 5), 7);   
easter_day = (22 + d + e);
if(easter_day/31>1)
    easter_day=easter_day-31;
    easter = datenum([day_vec(1) 04 easter_day 0 0 0]);
else
    easter = datenum([day_vec(1) 03 easter_day 0 0 0]);
end
%moveable holidays - easter related
holidays(2) = easter-2;
holidays(3) = easter+1;
holidays(4) = easter+39;
holidays(5) = easter+50;
%holidays(6) = easter+60;
%fixed holidays
holidays(1) = datenum([day_vec(1) 01 01 0 0 0]); %new year
holidays(7) = datenum([day_vec(1) 05 01 0 0 0]); %labour day
holidays(8) = datenum([day_vec(1) 10 03 0 0 0]); %german unity
holidays(9) = datenum([day_vec(1) 11 01 0 0 0]); %allhallows
holidays(10) = datenum([day_vec(1) 12 25 0 0 0]); %first day of chrismas
holidays(11) = datenum([day_vec(1) 12 26 0 0 0]); %second day of chrismas

sholidays(1) = datenum([day_vec(1) 12 24 0 0 0]); %christmas eve
sholidays(2) = datenum([day_vec(1) 12 31 0 0 0]); %new year's eve

if(~isempty(datefind(datenum(day_vec), holidays)))
    profile_number=tmp_number(1);
elseif(~isempty(datefind(datenum(day_vec), sholidays)))
    profile_number=tmp_number(3);
    if(strcmp(datestr(day_vec, 'dddd'), 'Sunday'))
        profile_number=tmp_number(1);
    end
else
%3. day selection
    day_str = datestr(day_vec, 'dddd');

    if(strcmp(day_str, 'Sunday'))
        profile_number = tmp_number(1);
    elseif(strcmp(day_str, 'Saturday'))
        profile_number = tmp_number(3);
    else
        profile_number = tmp_number(2);
    end    
end

function day_correction_factor = vdew_factor(date_num)
year = datevec(date_num);
fday = datenum([year(1) 01 01 0 0 0]);
days = daysact(fday,datenum(date_num))+1;
a=-3.92*10^-10;
b=3.2*10^-7;
c=-7.02*10^-5;
d=2.1*10^-3;
e=1.24;
day_correction_factor =a*(days^4)+b*(days^3)+c*(days^2)+d*(days^1)+e;

function ly = leapyear(year)
ly = 0;
if year > 1582
	if mod(year, 4) == 0
        ly = 1;
    end
	if mod(year, 100) == 0
        ly = 0;
    end
	if mod(year, 400) == 0
        ly = 1;
    end
end

% --- Executes on button press in corr_factor_checkbox.
function corr_factor_checkbox_Callback(hObject, eventdata, handles)
handles.GUI.correction_factor = get(handles.corr_factor_checkbox, 'Value');
guidata(hObject,handles);


% --- Executes on selection change in day_num_popup.
function day_num_popup_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
day = str2double(str{val});

val1=get(handles.houseno_popup,'Value');
str1=get(handles.houseno_popup,'String');
pos = str2double(str1{val1});

bar(handles.single_output_axes, handles.stor(pos).days(day).loadcurve);
if(isfield(handles, 'stor')) 
    handles = mod_single_plot(handles, pos, day);
end
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function day_num_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to day_num_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in day_num_norm_popup.
function day_num_norm_popup_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
day = str2double(str{val});

bar(handles.output_axes, handles.DATA.complete_loadcurve(day,:));
mod_plot(handles.output_axes, length(handles.DATA.complete_loadcurve(day,:)), 'load[W]');
set(handles.norm_load_text, 'string', [num2str((sum(handles.DATA.complete_loadcurve(day,:))/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
set(handles.norm_datum_text, 'string', datestr(handles.stor(1).days(day).date, 'yyyy-mm-dd'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function day_num_norm_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to day_num_norm_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
