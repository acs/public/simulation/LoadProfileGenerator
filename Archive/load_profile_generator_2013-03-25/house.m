%% Definitions for house class in this m-file
classdef house
   %% class properties
    properties
        ID % internal identifier
        profile % load, No. of events, No. of events per 1/4 hour
        event_stor; % eventstorage
        spec_estor;
        cooling_estor;
        users; % eventload
    end
    %% Class methods
    methods
        % constructor
        function obj=house(ID, profile, users)
            obj.ID=ID;
            obj.profile=profile;
            obj.event_stor=event(0,zeros(60,1));
            obj.spec_estor=event(0);
            obj.cooling_estor=event(0);
            obj.users=users;
            obj.profile.loadcurve = zeros(obj.profile.scale_used,1);
        end
        
        
        % house standby power eventgeneration and insertion
        function obj = standby_power_event(obj)
            % generate event
            standby_energy=obj.profile.usage_data*obj.profile.categories.num(10);
            obj.spec_estor(1) = event(1, 'standby', standby_energy*3600/obj.profile.scale_used/obj.profile.scale_input, obj.profile.scale_used);
            obj.spec_estor(1).to_use = 1;
            % insert event
            obj.profile.loadcurve(1:obj.profile.scale_used) = obj.profile.loadcurve(1:obj.profile.scale_used)+obj.spec_estor(1).profile.data;
        end
        
        
        % cooling and freezing event generation and insertion
        function obj = cooling_events(obj)
            
            %loop as long as uptime > 0.5 to calculate number of cycles
            cooling_energy = obj.profile.usage_data*obj.profile.categories.num(1);
            uptime = 1;
            runvar = 1;
            while(uptime > 0.5)
                cur_power = obj.users.std_list(1).num.min + rand*(obj.users.std_list(1).num.max-obj.users.std_list(1).num.min);
                obj.cooling_estor(runvar) = event(runvar, [obj.users.std_list(1).catname num2str(runvar)], cur_power*obj.users.std_list(1).distribution);
                runvar=runvar+1;
                cycle_energy=0;
                for i=1:length(obj.cooling_estor)
                    cycle_energy=cycle_energy+obj.cooling_estor(i).profile.energy;
                end
                cycles = floor(cooling_energy/cycle_energy)+1;
                for j=1:length(obj.cooling_estor)
                    obj.cooling_estor(j).profile.cycles = cycles;
                end
                uptime = cycles/96;
            end
            
            % cycle1: adaption of cycle energy
            temp=0;
            for k=1:length(obj.cooling_estor)
                temp = temp + obj.cooling_estor(k).profile.energy*obj.cooling_estor(k).profile.cycles;
            end
            delta_energy=temp-cooling_energy;
            new_energy=(obj.cooling_estor(1).profile.energy*obj.cooling_estor(1).profile.cycles-delta_energy)/obj.cooling_estor(1).profile.cycles;
            rescale_factor=new_energy/obj.cooling_estor(1).profile.energy;
            
            obj.cooling_estor(1).profile.data=rescale_factor*obj.cooling_estor(1).profile.data;
            obj.cooling_estor(1).profile.energy=new_energy;
            
            % insert events into profile
            for j=1:length(obj.cooling_estor)
                % first occurence defined by cycle count
                pos1=floor(rand*obj.profile.scale_used/obj.cooling_estor(j).profile.cycles)+1;
                % stepsize in relation to scale used
                step=round(obj.profile.scale_used/96/uptime);
                
                % adaptation of the profile to resulting profile scale
                dur = obj.cooling_estor(j).profile.duration;
                temp = zeros(floor(dur/obj.profile.scale_input)+1,1);

                for m=1:length(temp)-1
                    temp(m) = sum(obj.cooling_estor(j).profile.data( (m-1)*obj.profile.scale_input +1 : m*obj.profile.scale_input) )/obj.profile.scale_input;
                end
                temp(m+1) = sum(obj.cooling_estor(j).profile.data( m*obj.profile.scale_input +1 : dur) )/obj.profile.scale_input;
                obj.cooling_estor(j).profile.data = temp;
                obj.cooling_estor(j).profile.duration = length(temp);                
                
                len=obj.cooling_estor(j).profile.duration;
                
                for k=1:cycles
                    % if: error catching, when profile exceeds scale
                    % else: normal treatment
                    if(pos1>obj.profile.scale_used)
                        pos1=pos1-obj.profile.scale_used;
                        obj.profile.loadcurve(pos1:pos1+len-1) = obj.profile.loadcurve(pos1:pos1+len-1) + obj.cooling_estor(j).profile.data;
                        pos1=pos1+step;
                    elseif(pos1+len-1>obj.profile.scale_used)
                       
                        diff1 = obj.profile.scale_used-pos1;
                        obj.profile.loadcurve(pos1:obj.profile.scale_used) = obj.profile.loadcurve(pos1:obj.profile.scale_used) + obj.cooling_estor(j).profile.data(1:diff1+1);
                        obj.profile.loadcurve(1:len-(diff1+1))=obj.profile.loadcurve(1:len-(diff1+1))+ obj.cooling_estor(j).profile.data((diff1+1)+1:len);
                        
                        pos1=len-diff1+step ;

                    else
                        obj.profile.loadcurve(pos1:pos1+len-1) = obj.profile.loadcurve(pos1:pos1+len-1) + obj.cooling_estor(j).profile.data;
                        pos1=pos1+step;
                    end
                end
            end
        end
        
        
        % special event generation and insertion
        function obj = spec_events(obj)
            % generate special events

            runvar=length(obj.spec_estor);
            for i=2:9 % length(obj.users.spec_users)
                if(obj.users.spec_users.e_stor(i).to_use)
                    runvar=runvar+1;               
                    obj.spec_estor(runvar) = obj.users.spec_users.e_stor(i);

                    spec_energy = obj.profile.usage_data*obj.profile.categories.num(i);

                    usage_factor = spec_energy/(sum(obj.spec_estor(runvar).profile.data)/3600);

                    obj.spec_estor(runvar).profile.cycles = floor(usage_factor);
                    if(rand<mod(usage_factor,1))
                        obj.spec_estor(runvar).profile.cycles = obj.spec_estor(runvar).profile.cycles+1; 
                    end

                    % adaptation of the profile to resulting profile scale
                    dur = obj.spec_estor(runvar).profile.duration;
                    temp = zeros(floor(dur/obj.profile.scale_input)+1,1);

                    for m=1:length(temp)-1
                        temp(m) = sum(obj.spec_estor(runvar).profile.data( (m-1)*obj.profile.scale_input +1 : m*obj.profile.scale_input) )/obj.profile.scale_input;
                    end
                    % last value
                    temp(m+1) = sum(obj.spec_estor(runvar).profile.data( m*obj.profile.scale_input +1 : dur) )/obj.profile.scale_input;
                    
                    obj.spec_estor(runvar).profile.data = temp;
                    obj.spec_estor(runvar).profile.duration = length(temp);
                end
            end 
        end
        
        
        % continuous user-driven events generation and insertion
        function obj = continuous_events(obj)
            run_var=1;
            for i=2:(length(obj.profile.categories.num)-1)
                if(obj.profile.categories.num(i)>0 && ~obj.users.spec_users.e_stor(i).to_use)
                    cat_energy = obj.profile.categories.num(i)*obj.profile.usage_data;

                    cur_cat_energy = cat_energy;
                        
                    % Variante 1: rand + obere/untere Grenze von Kategorie
                    run_var2=1;
                    while(cur_cat_energy > 0)
                        dur_x = obj.users.std_list(i).duration(1) + floor(rand*(obj.users.std_list(i).duration(2)-obj.users.std_list(i).duration(1)));
                        dur_cur = 1;
                        cur_power = obj.users.std_list(i).num.min + rand*(obj.users.std_list(i).num.max-obj.users.std_list(i).num.min);
                        if(length(obj.users.std_list(i).distribution)>1)
                            dur_cur=dur_x;
                        end
                        obj.event_stor(run_var) = event(run_var, [obj.users.std_list(i).catname num2str(run_var2)], cur_power*obj.users.std_list(i).distribution(1:dur_cur), dur_x);

                        % cooking treatment
                        if(i==6)
                            % correlated probability between cooking events
                            cycles = floor(rand*3);
                            if(cycles>0)
                                temp_profile=obj.event_stor(run_var).profile.data;
                                dur_com=obj.event_stor(run_var).profile.duration;
                                for j=1:cycles
                                    dur_temp=floor(rand*(dur_com-600))+600;
                                    start_temp=floor(rand*(dur_com-dur_temp))+1;
                                    obj.event_stor(run_var).profile.data(start_temp:start_temp+dur_temp-1)=obj.event_stor(run_var).profile.data(start_temp:start_temp+dur_temp-1)+temp_profile(1:dur_temp);
                                end
                                obj.event_stor(run_var).profile.energy=sum(obj.event_stor(run_var).profile.data)/3600;
                            end
                            
                            %scale adaption if nessacary
                            dur=floor(obj.event_stor(run_var).profile.duration/obj.profile.scale_input);
                            temp = zeros(dur+1,1);
                            for m=1:length(temp)-1
                                temp(m) = sum(obj.event_stor(run_var).profile.data( (m-1)*obj.profile.scale_input +1 : m*obj.profile.scale_input) )/obj.profile.scale_input;
                            end
                            %last value
                            temp(m+1) = sum(obj.event_stor(run_var).profile.data( m*obj.profile.scale_input +1 : obj.event_stor(run_var).profile.duration) )/obj.profile.scale_input;
                            obj.event_stor(run_var).profile.data = temp;
                            obj.event_stor(run_var).profile.duration = dur;
                            obj.event_stor(run_var).to_use=1;
                        end
                        
                        cur_cat_energy = cur_cat_energy - obj.event_stor(run_var).profile.energy;
                        
                        run_var = run_var+1;
                        run_var2 = run_var2+1;
                    end

                    % reduction of energy consumption for critical event
                    run_var = run_var-1;
                    delta_energy=obj.event_stor(run_var).profile.energy+cur_cat_energy;
                    if(length(obj.event_stor(run_var).profile.data)==1)
                        obj.event_stor(run_var).profile.data=delta_energy*3600/obj.event_stor(run_var).profile.duration;
                        obj.event_stor(run_var).profile.energy = obj.event_stor(run_var).profile.duration*obj.event_stor(run_var).profile.data/3600;
                    else
                        rescale_factor=delta_energy/obj.event_stor(run_var).profile.energy;

                        obj.event_stor(run_var).profile.data=rescale_factor*obj.event_stor(run_var).profile.data;
                        obj.event_stor(run_var).profile.energy=delta_energy;
                    end
                    
                    run_var = run_var+1;
                end
            end
            % event mixing
            obj.event_stor = obj.event_stor(randperm(length(obj.event_stor)));
        end    
            
            
        
        function obj = insert_events(obj, vdew_rand_list)
            runvar=1;
            % special event insertion
            % insertion, number 1 is not necessary (standby event)
            for i=2:length(obj.spec_estor)
                for j=1:obj.spec_estor(i).profile.cycles
                    % startposition
                    startpos = vdew_rand_list(runvar);
                    len = length(obj.spec_estor(i).profile.data);
                    % ensures no parallel events of the same type
                    if(j>1)
                        temp_runvar=runvar+1;
                        while(~isempty(find(abs(startpos-obj.spec_estor(i).startpos)-obj.spec_estor(i).profile.duration<0, 1)))
                            temp_runvar=temp_runvar+1;
                            if(temp_runvar>length(vdew_rand_list))
                                break;
                            end
                            startpos = vdew_rand_list(temp_runvar);
                        end
                        temp_runvar=temp_runvar-1;
                        vdew_rand_list(temp_runvar) = [];
                        runvar=runvar-1;
                    end
                    
                    obj.spec_estor(i).startpos(j) = startpos;
                    runvar=runvar+1;
                    
                    if(startpos+len > obj.profile.scale_used)
                        diff = obj.profile.scale_used-startpos;
                        obj.profile.loadcurve(startpos:obj.profile.scale_used) = obj.profile.loadcurve(startpos:obj.profile.scale_used) + obj.spec_estor(i).profile.data(1:diff+1);
                        obj.profile.loadcurve(1:len-(diff+1))=obj.profile.loadcurve(1:len-(diff+1))+ obj.spec_estor(i).profile.data((diff+1)+1:len);
                    else
                        obj.profile.loadcurve(startpos:startpos+len-1) = obj.profile.loadcurve(startpos:startpos+len-1) + obj.spec_estor(i).profile.data;
                    end
                end
            end
            
            % continous event insertion
            % insertion
            if(obj.event_stor(1).ID>0)
                for i=1:length(obj.event_stor)
                    % startposition
                    startpos = vdew_rand_list(runvar);
                    
                    if(obj.event_stor(i).to_use)
                        if(isfield(obj.profile, 'cooking'))
                            temp_runvar=runvar;
                            while(~isempty(find(abs(startpos-obj.profile.cooking)-obj.event_stor(i).profile.duration<0, 1)))
                                temp_runvar=temp_runvar+1;
                                if(temp_runvar>length(vdew_rand_list))
                                    break;
                                end
                                startpos = vdew_rand_list(temp_runvar);
                            end
                            temp_runvar=temp_runvar-1;
                            vdew_rand_list(temp_runvar) = [];
                            runvar=runvar-1;
                            
                            obj.profile.cooking(length(obj.profile.cooking)+1)=startpos;
                        else
                            obj.profile.cooking(1) = startpos;
                        end
                    end
                    
                    runvar=runvar+1;                        
                    cur_elen=length(obj.event_stor(i).profile.data);
                    obj.event_stor(i).startpos = startpos;
                    if(cur_elen==1)
                        len = floor(obj.event_stor(i).profile.duration/obj.profile.scale_input);
                    else
                        len = cur_elen;
                    end

                    %full energy has to be used
                    if(cur_elen==1)
                        delta = obj.event_stor(i).profile.data*mod(obj.event_stor(i).profile.duration,obj.profile.scale_input)/obj.profile.scale_input;
                        if(startpos>obj.profile.scale_used)
                        obj.profile.loadcurve(1) = obj.profile.loadcurve(1)+delta;
                        else
                        obj.profile.loadcurve(startpos) = obj.profile.loadcurve(startpos)+delta;
                        end
                    end

                    if(startpos+len > obj.profile.scale_used)
                        diff = obj.profile.scale_used-startpos;
                        if(cur_elen==1)
                            obj.profile.loadcurve(startpos:obj.profile.scale_used) = obj.profile.loadcurve(startpos:obj.profile.scale_used) + obj.event_stor(i).profile.data;
                            obj.profile.loadcurve(1:len-(diff+1))=obj.profile.loadcurve(1:len-(diff+1))+ obj.event_stor(i).profile.data;
                        else
                            obj.profile.loadcurve(startpos:obj.profile.scale_used) = obj.profile.loadcurve(startpos:obj.profile.scale_used) + obj.event_stor(i).profile.data(1:diff+1);
                            obj.profile.loadcurve(1:len-(diff+1))=obj.profile.loadcurve(1:len-(diff+1))+ obj.event_stor(i).profile.data((diff+1)+1:len);
                        end
                    else
                        obj.profile.loadcurve(startpos:startpos+len-1) = obj.profile.loadcurve(startpos:startpos+len-1) + obj.event_stor(i).profile.data;
                    end
                end
            end
        end
        
        function eve_count = event_count(obj)
            eve_count = length(obj.event_stor)+length(obj.spec_estor);
        end
        
        % garbage colection to reduce memory usage
        function obj = garbage_collection(obj)
            temp = obj.profile.loadcurve;
            obj.profile = [];
            obj.users = [];
            obj.profile.loadcurve = temp;
        end
        
        function plot(obj)
            figure
            bar(obj.profile.loadcurve)
        end
      
    end
end

