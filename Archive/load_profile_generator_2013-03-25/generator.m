%% Step0 - GUI sensetive functions
% Initializing GUI, precalculating variables
function varargout = generator(varargin)
% GENERATOR MATLAB code for generator.fig
% See also: GUIDE, GUIDATA, GUIHANDLES

% initialization code - DO NOT EDIT --->
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @generator_OpeningFcn, ...
                   'gui_OutputFcn',  @generator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end % <---

% Executes just before generator is made visible, first(example) house is calculated.
function generator_OpeningFcn(hObject, eventdata, handles, varargin)

%import time-vector for export-fct (massive calculation time)
%and address vector for EXCEL columns 
handles.GUI = load('sources\excel_export.mat');

%loading VDEW and other data

handles.DATA.sprofile.data = importxls('sources\loadprofile_VDEW.xls', 1, 'H0');

[categories.num, categories.str] = importxls('sources\categories.xls',2 ,'categories');

handles.DATA.categories.prob  = categories.num(:,1);
handles.DATA.categories.avg = categories.num(:,2);
handles.DATA.categories.str = categories.str;

handles.GUI.categories.num = handles.DATA.categories.prob;
handles.GUI.categories.str = handles.DATA.categories.str;


handles.DATA.users = importxls('sources\userdata.xls', 3);

% special user import (returns struct with data...)
handles.DATA.users.spec_users.dishwasher = importxls('sources\dishwasher.xlsx', 4, 'dishwasher');
handles.DATA.users.spec_users.washing = importxls('sources\washingmashine.xlsx', 4, 'washingmashine');

% stores data to special event-class
handles.DATA.users.spec_users.e_stor(1) = event(01);
handles.DATA.users.spec_users.e_stor(2) = event(02);
handles.DATA.users.spec_users.e_stor(3) = event(03);
handles.DATA.users.spec_users.e_stor(4) = event(04);
handles.DATA.users.spec_users.e_stor(5) = event(05);
handles.DATA.users.spec_users.e_stor(6) = event(06);
handles.DATA.users.spec_users.e_stor(7) = event(07);
handles.DATA.users.spec_users.e_stor(8) = event(08, handles.DATA.users.spec_users.dishwasher.str, handles.DATA.users.spec_users.dishwasher.num, length(handles.DATA.users.spec_users.dishwasher.num));
setpushbuttonvisible(hObject, handles, 8, 1);
handles.DATA.users.spec_users.e_stor(9) = event(09, handles.DATA.users.spec_users.washing.str, handles.DATA.users.spec_users.washing.num, length(handles.DATA.users.spec_users.washing.num));
setpushbuttonvisible(hObject, handles, 9, 1);

% category profile definition
% cooling(cat 1) & timerange
cooling_profile_dur=15;
cooling_profile=zeros(60*cooling_profile_dur,1);
cooling_profile_len=length(cooling_profile);
cooling_profile(1:cooling_profile_len*2/cooling_profile_dur)=1;
cooling_profile(cooling_profile_len*2/cooling_profile_dur+1:cooling_profile_len)=.75;

% constant distribution (2/15: 1; 13/15: 0.75)
handles.DATA.users.std_list(1).distribution = cooling_profile;
% constant duration (15 minutes)
handles.DATA.users.std_list(1).duration = [cooling_profile_dur*60, cooling_profile_dur*60];

% PC/Comm(cat 2) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(2).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(2).duration = [5*60, 60*60];

% Audio/TV(cat 3) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(3).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(3).duration = [5*60, 60*60];

% Warm_water(cat 4) & timerange
% constant distribution (rectangle profile) [Conn Power high]
handles.DATA.users.std_list(4).distribution = 0.5;
% minimum duration: 0.5min ; maximum duration: 10min
handles.DATA.users.std_list(4).duration = [0.5*60, 10*60];

% light(cat 5) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(5).distribution = 1;
% minimum duration: 1min ; maximum duration: 10min
handles.DATA.users.std_list(5).duration = [1*60, 10*60];

% cooking(cat 6) & timerange 
cooking_profile = importxls('sources\cooking.xlsx', 1, 'cooking');
cooking_profile(:,1) = [];

% constant distribution (rectangle profile)
handles.DATA.users.std_list(6).distribution = cooking_profile;
% constant duration: 34min
handles.DATA.users.std_list(6).duration = [10*60, 60*60];
% electrical oven is only partionally used (20% - one plate)
handles.DATA.users.std_list(6).num.max = 1/5*handles.DATA.users.std_list(6).num.max;
handles.DATA.users.std_list(6).num.min = 1/5*handles.DATA.users.std_list(6).num.min;


% drying(cat 7) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(7).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(7).duration = [30*60, 60*60];

% dishwasher(cat 8) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(8).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(8).duration = [30*60, 60*60];

% washing(cat 9) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(9).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(9).duration = [30*60, 60*60];

% standby(cat 10) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(10).distribution = 1;
% standy constant over the day
handles.DATA.users.std_list(10).duration = [24*60*60, 24*60*60];




% GUI/DATA: Standard load profile and timescale
% set timescale to standard value 60 sec
handles.DATA.profile.scale = 60*60*24;
handles.DATA.profile.scale_input = 60;
handles.DATA.profile.vdew.scale_input = 60;
handles.DATA.profile.scale_used = 24/(handles.DATA.profile.scale_input/(60*60));

% used column
handles.DATA.sprofile.cur_col=11;

% initial generation of events
handles.DATA.sprofile.size = size(handles.DATA.sprofile.data);
handles.DATA.sprofile.sum = zeros(1,handles.DATA.sprofile.size(2)-1);
handles.DATA.sprofile.prob = zeros(handles.DATA.sprofile.size(1), handles.DATA.sprofile.size(2)-1);
handles.DATA.sprofile.prob_normed = zeros(handles.DATA.sprofile.size(1), handles.DATA.sprofile.size(2)-1);

% edit load profile to relative probability
for j=2:handles.DATA.sprofile.size(2)
handles.DATA.sprofile.cur_max=eps;
    for i=1:handles.DATA.sprofile.size(1)
        handles.DATA.sprofile.sum(j-1) = handles.DATA.sprofile.sum(j-1)+handles.DATA.sprofile.data(i,j);
        
        if(handles.DATA.sprofile.data(i,j)>handles.DATA.sprofile.cur_max)
            handles.DATA.sprofile.cur_max=handles.DATA.sprofile.data(i,j);
        end        
    end
    for i=1:handles.DATA.sprofile.size(1)
        handles.DATA.sprofile.prob(i,j-1) = handles.DATA.sprofile.data(i,j)/handles.DATA.sprofile.sum(j-1);
        %norming to one, not used
        handles.DATA.sprofile.prob_normed(i,j-1) = handles.DATA.sprofile.prob(i,j-1)/(handles.DATA.sprofile.cur_max/handles.DATA.sprofile.sum(j-1));
    end
end

% set initial load
handles.DATA.profile.data = handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1);
handles.DATA.profile.usage_def = 2740;
handles.DATA.profile.usage_data = handles.DATA.profile.usage_def;

% stores load to special variable
handles.DATA.profile.categories.num=handles.DATA.categories.prob;



% GUI-textfield refreshing: length of special events
for l=1:length(handles.DATA.users.spec_users.e_stor)
    len = length(handles.DATA.users.spec_users.e_stor(l).profile.data);
    set(handles.(genvarname(['cat' num2str(l) '_ltext'])), 'string', sec2timestr(len));
end

temp_sum = 0;
len=length(handles.DATA.categories.str);

for i=1:len
    % get used categories from checkboxes
    handles.GUI.categories.checkbox(i) = get(handles.(genvarname(['cat' num2str(i) '_checkbox'])), 'Value');
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end

for j=1:length(handles.DATA.categories.prob)
    if(handles.GUI.categories.checkbox(j))
        handles.DATA.profile.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
    else
        handles.DATA.profile.categories.num(j) = 0;
    end
end

% update category load text fields
for k=1:len
    set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.profile.categories.num(k)*100, '%7.2f'), ' %']);
    set(handles.(genvarname(['cat' num2str(k) '_static_text'])), 'string', ['(' num2str(handles.DATA.categories.prob(k)*100, '%7.2f'), '%)']);
    set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.profile.usage_data*handles.DATA.profile.categories.num(k), '%7.1f'), 'Wh']);
end

% pre-calculates vdew probability distribution
handles = premeasure_vdewdistribution(handles);


% DATA:
% create a first house with std data
handles.first = house(0, handles.DATA.profile, handles.DATA.users);

handles.first = handles.first.standby_power_event();
handles.first = handles.first.cooling_events();
handles.first = handles.first.continuous_events();
%handles.first = handles.first.spec_events(); no measured devices
handles.first = handles.first.insert_events(vdew_rand(handles.DATA.profile.vdew, (handles.first.event_count()+20)));

% GUI: plot first results
% plot used standard loadprofile
bar(handles.input_axes, handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1));

% plot output profile with the standard house data
bar(handles.output_axes, handles.first.profile.loadcurve);
bar(handles.single_output_axes, handles.first.profile.loadcurve);

% modify plots for right axes assignment
mod_plot(handles.output_axes, length(handles.first.profile.loadcurve), 'load[W]');
mod_plot(handles.single_output_axes, length(handles.first.profile.loadcurve), 'load[W]');
mod_plot(handles.input_axes, length(handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1)), 'rel. probability');

% set energy text field to standard value
set(handles.norm_load_text, 'string', [num2str((sum(handles.first.profile.loadcurve)/3600*handles.DATA.profile.scale_input),'%7.1f'), ' Wh']);
set(handles.sing_load_text, 'string', [num2str((sum(handles.first.profile.loadcurve)/3600*handles.DATA.profile.scale_input),'%7.1f'), ' Wh']);

% Choose default command line output for generator
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = generator_OutputFcn(hObject, eventdata, handles) 
% no interaction with the command line, except error handling
varargout{1} = handles.output;

%% Step1 - VDEW selection/power demand selection functions
% function changes the variable for used standardloadprofile and plots the used standard loadprofile
function select_data_popup_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
% based on dropdown user input the VDEW STDLP is chosen
switch str{val}
    case 'normalized(std)'
        handles.DATA.sprofile.cur_col=11;
    case 'winter_workday'
        handles.DATA.sprofile.cur_col=4;
    case 'winter_saturday'
        handles.DATA.sprofile.cur_col=2;
    case 'winter_sunday'
        handles.DATA.sprofile.cur_col=3;
    case 'interim_period_workday'
        handles.DATA.sprofile.cur_col=10;
    case 'interim_period_saturday'
        handles.DATA.sprofile.cur_col=8;
    case 'interim_period_sunday'
        handles.DATA.sprofile.cur_col=9;
    case 'summer_workday'
        handles.DATA.sprofile.cur_col=7;
    case 'summer_saturday'
        handles.DATA.sprofile.cur_col=5;
    case 'summer_sunday'
        handles.DATA.sprofile.cur_col=6;
end
% updates the handles structure
handles = premeasure_vdewdistribution(handles);

% Update plot and handles structure
set(handles.calc_cat_text, 'string', 'parameter changed');
bar(handles.input_axes, handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1))
mod_plot(handles.input_axes, length(handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1)), 'rel. probability');

guidata(hObject,handles);

% Change daily load consumption
function day_load_edit_Callback(hObject, eventdata, handles)
cur_load = str2double(get(handles.day_load_edit,'String'));
if(isnan(cur_load))
    cur_load = 2740;
    set(handles.day_load_text, 'string', 'NaN, used 2740 Wh(std)');
else
    set(handles.day_load_text, 'string', [num2str(cur_load), ' Wh']);
end
handles.DATA.profile.usage_data = cur_load;
set(handles.calc_cat_text, 'string', 'parameter changed');

guidata(hObject,handles);

% popupmenu selection of used timescale
function timescale_popupmenu_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
switch str{val}
    case '1 sec'
        handles.DATA.profile.scale_input = 1;
        handles.DATA.profile.vdew.scale_input = 1;
    case '2 sec'
        handles.DATA.profile.scale_input = 2;
        handles.DATA.profile.vdew.scale_input = 2;
    case '5 sec'
        handles.DATA.profile.scale_input = 5;
        handles.DATA.profile.vdew.scale_input = 5;
    case '10 sec'
        handles.DATA.profile.scale_input = 10;
        handles.DATA.profile.vdew.scale_input = 10;
    case '15 sec'
        handles.DATA.profile.scale_input = 15;
        handles.DATA.profile.vdew.scale_input = 15;
    case '20 sec'
        handles.DATA.profile.scale_input = 20;
        handles.DATA.profile.vdew.scale_input = 20;
    case '30 sec'
        handles.DATA.profile.scale_input = 30;
        handles.DATA.profile.vdew.scale_input = 30;
    case '60 sec'
        handles.DATA.profile.scale_input = 60;
        handles.DATA.profile.vdew.scale_input = 60;
end

handles.DATA.profile.scale_used = 24/(handles.DATA.profile.scale_input/(60*60));

% Update handles structure
guidata(hObject,handles);

%% Step2 - Category selection and calculation
% execute after applying all inputs for category settings by users command
function calc_cat_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles);

temp_sum = 0;

for i=1:length(handles.GUI.categories.checkbox)
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end

if(temp_sum == 0)
    set(handles.calc_cat_text, 'string', '0 - failed');
else
    for j=1:length(handles.DATA.categories.prob)
        if(handles.GUI.categories.checkbox(j))
            handles.DATA.profile.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
        else
            handles.DATA.profile.categories.num(j) = 0;
        end
    end
    
    handles = premeasure_vdewdistribution(handles);

    % cat load text-field update
    for k=1:length(handles.DATA.categories.prob)
        if(strcmp(get(handles.(genvarname(['cat' num2str(k) '_axes'])), 'Visible'), 'on'))
            handles.DATA.users.spec_users.e_stor(k).to_use=1;
            % cycle refresh
            suma=sum(handles.DATA.users.spec_users.e_stor(k).profile.data)/3600;
            usage_factor=(handles.DATA.profile.usage_data*handles.DATA.profile.categories.num(k))/suma;
            set(handles.(genvarname(['cat' num2str(k) '_consumtext'])), 'String', [num2str(suma/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
        end
        set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.profile.usage_data*handles.DATA.profile.categories.num(k), '%7.1f'), 'Wh']);
        set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.profile.categories.num(k)*100, '%7.2f'), ' %']);
    end

    set(handles.calc_cat_text, 'string', 'calc done');
    
end
busy_switch(handles);
guidata(hObject,handles);

% helpfunction for random probability distribution function
function handles = premeasure_vdewdistribution(handles)

% overwrites used data for event spreading
handles.DATA.profile.data = handles.DATA.sprofile.prob(:,handles.DATA.sprofile.cur_col-1);

handles.DATA.profile.data = handles.DATA.profile.data-(handles.DATA.profile.categories.num(1)+handles.DATA.profile.categories.num(10))/96;
% if prob<0 --> prob=0
handles.DATA.profile.data(find(handles.DATA.profile.data<0))=0;
vdew_reduced_sum=sum(handles.DATA.profile.data);

handles.DATA.profile.data=handles.DATA.profile.data/vdew_reduced_sum;

len=10000;
handles.DATA.profile.vdew.intervall=zeros(len,1);
handles.DATA.profile.vdew.rescaled=round(handles.DATA.profile.data*len);
handles.DATA.profile.vdew.startpos=zeros(length(handles.DATA.profile.vdew.rescaled),1);

% gen...
handles.DATA.profile.vdew.startpos(1)=1;
handles.DATA.profile.vdew.intervall(handles.DATA.profile.vdew.startpos(1):handles.DATA.profile.vdew.rescaled(1))=1;
for i=2:length(handles.DATA.profile.data)
   handles.DATA.profile.vdew.startpos(i)=handles.DATA.profile.vdew.startpos(i-1)+handles.DATA.profile.vdew.rescaled(i-1);
   handles.DATA.profile.vdew.intervall(handles.DATA.profile.vdew.startpos(i):(handles.DATA.profile.vdew.startpos(i)+handles.DATA.profile.vdew.rescaled(i)))=i;
end
% error catching
if((handles.DATA.profile.vdew.startpos(i)+handles.DATA.profile.vdew.rescaled(i))<length(handles.DATA.profile.vdew.intervall))
    handles.DATA.profile.vdew.intervall(handles.DATA.profile.vdew.startpos(i)+handles.DATA.profile.vdew.rescaled(i)+1:length(handles.DATA.profile.vdew.intervall))=i;
end

% returns a random number based on vdew propability distribution
function startpos_list = vdew_rand(vdew, count)
startpos_list=zeros(count,1);
for i=1:count
    pos_y = floor(rand*length(vdew.intervall))+1;
    pos_x = vdew.intervall(pos_y);
    frac = (pos_y-vdew.startpos(pos_x))/vdew.rescaled(pos_x);
    startpos_list(i) = floor((pos_x-1+frac)*15*60/vdew.scale_input)+1;
end


%% Step3 - Main function for simulation 
% Executes loop for #houses
function plot_complete_pushbutton_Callback(hObject, eventdata, handles)

    busy_switch(handles);
    handles.no_houses = str2double(get(handles.input_houses_edit,'String'));
    handles.complete_load = zeros(handles.DATA.profile.scale_used, 1);
    
    if(isnan(handles.no_houses))
        handles.no_houses = 1;
    end
    
    set(handles.stop_pushbutton, 'Visible', 'on');
    set(handles.stop_text, 'Visible', 'off');
    
    h_run_var = 0;
    handles.stopprocess = 0;
    
tic % time counter start tag
for i = 1:handles.no_houses
    if(strcmp(get(handles.stop_text, 'String'), 'STOP'))
        break;
    end
    h_run_var = h_run_var + 1;

    handles.stor(i) = house(i, handles.DATA.profile, handles.DATA.users);
    handles.stor(i) = handles.stor(i).standby_power_event();
    if(handles.GUI.categories.checkbox(1))
        handles.stor(i) = handles.stor(i).cooling_events();
    end
    handles.stor(i) = handles.stor(i).spec_events();
    handles.stor(i) = handles.stor(i).continuous_events();
    handles.stor(i) = handles.stor(i).insert_events(vdew_rand(handles.DATA.profile.vdew, handles.stor(i).event_count()+40));

    % generate complete load curve
    handles.complete_load = handles.complete_load + handles.stor(i).profile.loadcurve;

    % progressbar update
    uiprogressbar(i, handles.no_houses, handles);
end

    if(h_run_var==handles.no_houses)
        set(handles.stop_text, 'string', 'finished');
    else
        set(handles.stop_text, 'string', 'stopped');
    end
    
    set(handles.stop_pushbutton, 'Visible', 'off');
    set(handles.stop_text, 'Visible', 'on');
    
    if (h_run_var == 0)
        h_run_var = 1;
    end
        
    handles.complete_load = handles.complete_load/h_run_var;
    
    set(handles.xls_export1_edit, 'String', num2str(1));
    set(handles.xls_export2_edit, 'String', num2str(h_run_var));

    bar(handles.output_axes, handles.complete_load);
    mod_plot(handles.output_axes, length(handles.complete_load), 'load[W]');
    
    % text update
    set(handles.calc_cat_text, 'string', '');
    
    set(handles.no_houses_text, 'string', ['ploted for ', num2str(h_run_var), ' houses']);
    set(handles.norm_load_text, 'string', [num2str((sum(handles.complete_load)/3600*handles.DATA.profile.scale_input),'%7.1f'), ' Wh']);

    
    % single plot refresh
    for n=1:h_run_var
        no_str(n).num = num2str(n);
        %no_str(n).num = ['House No. ',num2str(n)];
    end
    ce_str = struct2cell(no_str);
    set(handles.houseno_popup, 'String', ce_str);
    
    bar(handles.single_output_axes, handles.stor(1).profile.loadcurve);
    handles = mod_single_plot(handles, 1);
    
busy_switch(handles);

guidata(hObject,handles);

% Callback for stop button, dialog for confim
function stop_pushbutton_Callback(hObject, eventdata, handles)
selection = questdlg('Do you want to stop this process?',...
                     'Stop process',...
                     'Yes','No','Yes');
switch selection,
   case 'Yes',
         set(handles.stop_text, 'String', 'STOP');
         drawnow;
   case 'No'
         return
end


%% Step4 - functions to display & save results
% Callback function for single plot update by dropdownmenu selection
function houseno_popup_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');

pos = str2double(str{val});
bar(handles.single_output_axes, handles.stor(pos).profile.loadcurve);
if(isfield(handles, 'stor')) 
    handles = mod_single_plot(handles, pos);
end
guidata(hObject,handles);

% helpfunction for modification of single household plot
function handles = mod_single_plot(handles, pos)
runvar=1;
hold on;
for m=1:length(handles.stor(pos).spec_estor)
    if(handles.stor(pos).spec_estor(m).to_use)
        for i=1:length(handles.stor(pos).spec_estor(m).startpos)
            rndclr=[rand, rand, rand];
            if(handles.stor(pos).spec_estor(m).startpos(i)+handles.stor(pos).spec_estor(m).profile.duration>handles.stor(pos).profile.scale_used)
                plot([handles.stor(pos).spec_estor(m).profile.data(handles.stor(pos).profile.scale_used-handles.stor(pos).spec_estor(m).startpos(i)+1:handles.stor(pos).spec_estor(m).profile.duration); zeros((handles.stor(pos).profile.scale_used-handles.stor(pos).spec_estor(m).profile.duration), 1); handles.stor(pos).spec_estor(m).profile.data(1:handles.stor(pos).profile.scale_used-handles.stor(pos).spec_estor(m).startpos(i))], 'color', rndclr);
            else
                plot([zeros(handles.stor(pos).spec_estor(m).startpos(i), 1); handles.stor(pos).spec_estor(m).profile.data; zeros(handles.stor(pos).profile.scale_used-(handles.stor(pos).spec_estor(m).startpos(i)+handles.stor(pos).spec_estor(m).profile.duration),1)], 'color', rndclr);
            end
            runvar=runvar+1;
        end
    end
end
hold off;

mod_plot(handles.single_output_axes, length(handles.stor(pos).profile.loadcurve), 'load[W]');
set(handles.houseno_text, 'string', ['House ', num2str(pos), ' plotted']);
set(handles.sing_load_text, 'string', [num2str((sum(handles.stor(pos).profile.loadcurve)/3600*handles.DATA.profile.scale_input),'%7.1f'), ' Wh']);



% saves handles structure to .mat file
function save_data_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('handles_v','Save your GUI data');

if pathname == 0 %if the user pressed cancelled, then exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname, filename); 
busy_switch(handles);
%FileName = fullfile(saveDataName, 'Handles.mat');
save(saveDataName, 'handles'); 
busy_switch(handles);

% TXT export function
function save_data2txt_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the file
[filename,pathname] = uiputfile('*.txt','Saving data to text file...','load_v');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
busy_switch(handles);
%construct the path name of the save location
saveName = fullfile(pathname,filename); 

% TEXT EXPORT STARTS -->
f_ID = fopen(saveName,'w');
l_houses=length(handles.stor);
pro_len=length(handles.complete_load);

%create the first line
fprintf(f_ID,'%-8s','Time');
for i=1:l_houses
    cur_house = ['House' num2str(i)];
    fprintf(f_ID,'\t%-8s', cur_house);
end
fprintf(f_ID,'\n');

for z=1:pro_len
    fprintf(f_ID,'%-8s',handles.GUI.time(86400/pro_len).str{z,1});
    for i=1:l_houses
        fprintf(f_ID,'\t%-8.2f', handles.stor(i).profile.loadcurve(z,1));
    end
    fprintf(f_ID,'\n');
end

fclose(f_ID);
% TEXT EXPORT ENDS <--
busy_switch(handles);
guidata(hObject,handles);


% saves a calculation to a .fig file
function save_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('guidata_v','Save your GUI settings');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname,filename); 
busy_switch(handles);
%saves the gui data
hgsave(saveDataName);
busy_switch(handles);

% loads a saved calculation and closes the old
function load_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to choose which settings to load
[filename, pathname] = uigetfile('*.fig', 'Choose the GUI settings file to load');

%construct the path name of the file to be loaded
loadDataName = fullfile(pathname,filename);

%this is the gui that will be closed once we load the new settings
theCurrentGUI = gcf;  

%load the settings, which creates a new gui
hgload(loadDataName); 

%closes the old gui
close(theCurrentGUI);

% export function XLS
function xls_export_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the ,file
[filename,pathname] = uiputfile('*.xlsx','Save your House Data to xls');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname,filename); 

busy_switch(handles);

len = length(handles.complete_load);
xlswrite(saveDataName, handles.GUI.time(86400/len).str, 'comp_load', ['A1:A' num2str(len)]);
xlswrite(saveDataName, handles.complete_load, 'comp_load', ['B1:B' num2str(len)]);

startp = str2double(get(handles.xls_export1_edit,'String'));
endp = str2double(get(handles.xls_export2_edit,'String'));

addr_len = length(handles.GUI.excel_export);
if(isfield(handles, 'stor'))
    runvar=startp;
    for i=1:floor(endp/addr_len)
        for col=1:addr_len
        xlswrite(saveDataName, handles.stor(runvar).profile.loadcurve, ['houses' num2str(i)], [char(handles.GUI.excel_export(col)) '1:' char(handles.GUI.excel_export(col)) num2str(len)]);
        runvar=runvar+1;
        end
    end
    
    for col=1:mod(endp, addr_len) 
        xlswrite(saveDataName, handles.stor(runvar).profile.loadcurve, ['houses' num2str(i+1)], [char(handles.GUI.excel_export(col)) '1:' char(handles.GUI.excel_export(col)) num2str(len)]);
        runvar=runvar+1;
    end
else
    xlswrite(saveDataName, handles.first.profile.loadcurve, ['house_' 'first']);
end
busy_switch(handles);

% time string generation - excessive time need - atm: not in use
function outp = temp(scale_input)
outp{1,1} = datestr([2011,1,1,0,0,0], 'HH:MM:SS');
for i=2:scale_input
    %datestr(datenum(datestr([2009,1,1,11,7,18], 'HH:MM:SS'))+1/(24*3600)*86400/scale_input, 'HH:MM:SS')
    
    tmp = datenum(outp{i-1,1});  
    outp{i,1} = datestr(tmp+1/scale_input, 'HH:MM:SS');
end


%% helpfunctions for GUI apperance

% function modifies a plot axes based on input parameters
function mod_plot(hand, leng, ystr)
% x-axis
set(hand,'XLim', [0 leng]);
set(hand,'XTick', [0:leng/24:leng]);
set(hand,'XTickLabel',[' 0';'  ';' 2';'  ';' 4';'  ';' 6';'  ';' 8';'  ';'10';'  ';'12';'  ';'14';'  ';'16';'  ';'18';'  ';'20';'  ';'22';'  ';'24']);
set(get(hand, 'XLabel'), 'String' ,'time[h]');
set(hand,'XGrid','on');
% y-axis
set(get(hand, 'YLabel'), 'String' ,ystr);
set(hand,'yGrid','on');

% changes visibility for categories / special event pushbuttons
function cat1_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 1, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat2_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 2, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat3_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 3, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat4_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 4, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat5_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 5, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat6_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 6, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat7_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 7, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat8_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 8, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat9_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 9, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');

function cat10_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 10, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function setpushbuttonvisible(hObject, handles, nr, vis)
handles.GUI.categories.checkbox(nr) = get(handles.(genvarname(['cat' num2str(nr) '_checkbox'])), 'Value');
if(vis)
    status = get(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible');
    status2 = get(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible');
    if(strcmp(status, 'on') && strcmp(status2, 'off'))
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    elseif(strcmp(status2, 'on') && ~strcmp(status, 'on'))
        xbuttonupdate(hObject, handles, nr);
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    else
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');    
    end
end
guidata(hObject,handles);

% updates textfields and applies appearance of axes for special-events
function cat1_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 1);

function cat2_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 2);

function cat3_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 3);

function cat4_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 4);

function cat5_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 5);

function cat6_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 6);

function cat7_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 7);

function cat8_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 8);

function cat9_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 9);

function cat10_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 10);

function mod_spec_load(handles, num)
set(handles.(genvarname(['cat' num2str(num) '_spec_pushbutton'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(num) '_axes'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_x'])), 'Visible', 'on');
bar(handles.(genvarname(['cat' num2str(num) '_axes'])), handles.DATA.users.spec_users.e_stor(num).profile.data);

handles.DATA.users.spec_users.e_stor(num).to_use=1;

leng=length(handles.DATA.users.spec_users.e_stor(num).profile.data);
maxi=max(handles.DATA.users.spec_users.e_stor(num).profile.data);
suma=sum(handles.DATA.users.spec_users.e_stor(num).profile.data)/3600;
usage_factor=(handles.DATA.profile.usage_data*handles.DATA.profile.categories.num(num))/suma;
set(handles.(genvarname(['cat' num2str(num) '_ltext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XLim', [0 leng]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTick', 0:leng:leng);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YLim', [0 (maxi*1.1)]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTick', 0:maxi:maxi);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'String', [num2str(suma/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
set(handles.calc_cat_text, 'string', 'parameter changed');

% updates the visibility of the special event use buttons
function cat1_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 1);

function cat2_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 2);

function cat3_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 3);

function cat4_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 4);

function cat5_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 5);

function cat6_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 6);

function cat7_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 7);

function cat8_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 8);

function cat9_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 9);

function cat10_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 10);

function xbuttonupdate(hObject, handles, nr)

set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_x'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_ltext'])), 'Visible', 'off');
cla(handles.(genvarname(['cat' num2str(nr) '_axes']))); %clears axis-objects
set(handles.(genvarname(['cat' num2str(nr) '_consumtext'])), 'Visible', 'off');
handles.DATA.users.spec_users(nr).to_use=0;

set(handles.calc_cat_text, 'string', 'parameter changed');
guidata(hObject,handles);

% guibusy text visibility
function busy_switch(handles)
% get current ui status
status = get(handles.guibusy_text, 'Visible');
% changes visibility/arrow design
if(strcmp(status,'off')) 
    set(gcf, 'Pointer', 'watch');
    set(handles.guibusy_text, 'Visible', 'on');
else
    set(gcf, 'Pointer', 'arrow');
    set(handles.guibusy_text, 'Visible', 'off');
end
drawnow;

% progressbar update function
function uiprogressbar(x, z, handles)
frac = x/z;
pos = get(handles.progressbar_text,'position');
pos2 = get(handles.progress_text, 'position');
% bar position & length calculation
pos(3) = frac*(pos2(3)-2);
set(handles.progressbar_text,'position',pos,'string',sprintf('%.0f%%',frac*100))
outp = sec2timestr(fix(toc*(z-x)/x));
set(handles.progressstatus_text, 'string', [outp, ' remaining']);
drawnow

% this function returns string for time left - input: time left in seconds
function timestr = sec2timestr(sec)
% Convert a time measurement from seconds into a human readable string.

% Convert seconds to other units
w = floor(sec/604800); % Weeks
sec = sec - w*604800;
d = floor(sec/86400); % Days
sec = sec - d*86400;
h = floor(sec/3600); % Hours
sec = sec - h*3600;
m = floor(sec/60); % Minutes
sec = sec - m*60;
s = floor(sec); % Seconds

% Create time string
if w > 0
    if w > 9
        timestr = sprintf('%d week', w);
    else
        timestr = sprintf('%d week, %d day', w, d);
    end
elseif d > 0
    if d > 9
        timestr = sprintf('%d day', d);
    else
        timestr = sprintf('%d day, %d hr', d, h);
    end
elseif h > 0
    if h > 9
        timestr = sprintf('%d hr', h);
    else
        timestr = sprintf('%d hr, %d min', h, m);
    end
elseif m > 0
    if m > 9
        timestr = sprintf('%d min', m);
    else
        timestr = sprintf('%d min, %d sec', m, s);
    end
else
    timestr = sprintf('%d sec', s);
end



%% All 'unused' callback functions down here (EDIT Boxes)

% Executes during object creation, after setting all properties. All create functions down here
% popup menues 
function select_data_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function houseno_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function timescale_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% edit boxes
function day_load_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_houses_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xls_export1_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function xls_export2_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% unused callbacks
function input_houses_edit_Callback(hObject, eventdata, handles)

function xls_export1_edit_Callback(hObject, eventdata, handles)

function xls_export2_edit_Callback(hObject, eventdata, handles)
