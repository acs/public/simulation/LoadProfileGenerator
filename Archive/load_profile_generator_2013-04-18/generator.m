%% Step0 - GUI sensetive functions
% Initializing GUI, precalculating variables
function varargout = generator(varargin)
% GENERATOR MATLAB code for generator.fig
% See also: GUIDE, GUIDATA, GUIHANDLES

% initialization code - DO NOT EDIT --->
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @generator_OpeningFcn, ...
                   'gui_OutputFcn',  @generator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end % <--- DO NOT EDIT

% Executes just before generator figure is made visible, first(example) house is calculated.
function generator_OpeningFcn(hObject, eventdata, handles, varargin)
%loading VDEW and other data
handles.DATA.SLP.data = importxls('sources\loadprofile_VDEW.xls', 1, 'H0');
[categories.num, categories.str] = importxls('sources\categories.xls',2 ,'categories');
handles.DATA.users = importxls('sources\userdata.xls', 3);

% edit the imported data to prepare further calculations
handles.DATA.categories.prob  = categories.num(:,1);
handles.DATA.categories.avg = categories.num(:,2);
handles.DATA.categories.str = categories.str;

handles.GUI.categories.num = handles.DATA.categories.prob;
handles.GUI.categories.str = handles.DATA.categories.str;

% special user import (returns struct with data...)
handles.DATA.users.spec_users.dishwasher = importxls('sources\dishwasher.xlsx', 4, 'dishwasher');
handles.DATA.users.spec_users.washing = importxls('sources\washingmachine.xlsx', 4, 'washingmachine');

% stores data to special event-class
handles.DATA.users.spec_users.e_stor(1) = event(01);
handles.DATA.users.spec_users.e_stor(2) = event(02);
handles.DATA.users.spec_users.e_stor(3) = event(03);
handles.DATA.users.spec_users.e_stor(4) = event(04);
handles.DATA.users.spec_users.e_stor(5) = event(05);
handles.DATA.users.spec_users.e_stor(6) = event(06);
handles.DATA.users.spec_users.e_stor(7) = event(07);
handles.DATA.users.spec_users.e_stor(8) = event(08, handles.DATA.users.spec_users.dishwasher.str, handles.DATA.users.spec_users.dishwasher.num, length(handles.DATA.users.spec_users.dishwasher.num));
setpushbuttonvisible(hObject, handles, 8, 1);
handles.DATA.users.spec_users.e_stor(9) = event(09, handles.DATA.users.spec_users.washing.str, handles.DATA.users.spec_users.washing.num, length(handles.DATA.users.spec_users.washing.num));
setpushbuttonvisible(hObject, handles, 9, 1);

% category profile definition (relative power): 15min: first 2min: 100%, remaining 13min: 75%  
% cooling(cat 1) & timerange
cooling_profile_dur=15;
cooling_profile=zeros(60*cooling_profile_dur,1);
cooling_profile_len=length(cooling_profile);
cooling_profile(1:cooling_profile_len*2/cooling_profile_dur)=1;
cooling_profile(cooling_profile_len*2/cooling_profile_dur+1:cooling_profile_len)=.75;

% constant distribution (2/15: 1; 13/15: 0.75)
handles.DATA.users.std_list(1).distribution = cooling_profile;
% constant duration (15 minutes)
handles.DATA.users.std_list(1).duration = [cooling_profile_dur*60, cooling_profile_dur*60];

% PC/Comm(cat 2) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(2).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(2).duration = [5*60, 60*60];

% Audio/TV(cat 3) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(3).distribution = 1;
% minimum duration: 5min ; maximum duration: 60min
handles.DATA.users.std_list(3).duration = [5*60, 60*60];

% Warm_water(cat 4) & timerange
% constant distribution (rectangle profile) [Conn Power high]
handles.DATA.users.std_list(4).distribution = 0.5;
% minimum duration: 0.5min ; maximum duration: 10min
handles.DATA.users.std_list(4).duration = [0.5*60, 10*60];

% lighting(cat 5) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(5).distribution = 1;
% minimum duration: 1min ; maximum duration: 10min
handles.DATA.users.std_list(5).duration = [1*60, 10*60];

% cooking(cat 6) & timerange 
cooking_profile = importxls('sources\cooking.xlsx', 1, 'cooking');
cooking_profile(:,1) = [];

% constant distribution (rectangle profile)
handles.DATA.users.std_list(6).distribution = cooking_profile;
% equally distributed duration between 10min and 60min
handles.DATA.users.std_list(6).duration = [10*60, 60*60];
% electrical oven is only partionally used (20% - one plate)
handles.DATA.users.std_list(6).num.max = 1/5*handles.DATA.users.std_list(6).num.max;
handles.DATA.users.std_list(6).num.min = 1/5*handles.DATA.users.std_list(6).num.min;


% drying(cat 7) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(7).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(7).duration = [30*60, 60*60];

% dishwasher(cat 8) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(8).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(8).duration = [30*60, 60*60];

% washing(cat 9) & timerange
% constant distribution (rectangle profile)
handles.DATA.users.std_list(9).distribution = 1;
% minimum duration: 30min ; maximum duration: 60min
handles.DATA.users.std_list(9).duration = [30*60, 60*60];

% standby(cat 10) & timerange
% constant distribution (rectangular profile)
handles.DATA.users.std_list(10).distribution = 1;
% standy constant over the day
handles.DATA.users.std_list(10).duration = [24*60*60, 24*60*60];

% GUI/DATA: Standard load profile and timescale
% set timescale to standard value 60 sec
handles.DATA.GUIdata.scale = 60*60*24;
handles.DATA.GUIdata.scale_input = 60;
handles.DATA.GUIdata.scale_used = 24/(handles.DATA.GUIdata.scale_input/(60*60));

% initialize SLP struct
handles.DATA.SLP.size = size(handles.DATA.SLP.data);
handles.DATA.SLP.sum = zeros(1,handles.DATA.SLP.size(2)-1);
handles.DATA.SLP.prob = zeros(handles.DATA.SLP.size(1), handles.DATA.SLP.size(2)-1);
handles.DATA.SLP.prob_normed = zeros(handles.DATA.SLP.size(1), handles.DATA.SLP.size(2)-1);

% VDEW --> propability function
for j=2:handles.DATA.SLP.size(2)
handles.DATA.SLP.cur_max=eps;
    for i=1:handles.DATA.SLP.size(1)
        handles.DATA.SLP.sum(j-1) = handles.DATA.SLP.sum(j-1)+handles.DATA.SLP.data(i,j);
        
        if(handles.DATA.SLP.data(i,j)>handles.DATA.SLP.cur_max)
            handles.DATA.SLP.cur_max=handles.DATA.SLP.data(i,j);
        end        
    end
    for i=1:handles.DATA.SLP.size(1)
        handles.DATA.SLP.prob(i,j-1) = handles.DATA.SLP.data(i,j)/handles.DATA.SLP.sum(j-1);
        %norming to one, not used
        %handles.DATA.SLP.prob_normed(i,j-1) = handles.DATA.SLP.prob(i,j-1)/(handles.DATA.SLP.cur_max/handles.DATA.SLP.sum(j-1));
    end
end
% yearly consumption is set to 1000kWh (1Mio Wh)
handles.DATA.GUIdata.usage_data = 1000000;

% stores load to special variable
handles.DATA.GUIdata.categories.num=handles.DATA.categories.prob;

% GUI-text refreshing: length of special events
handles.GUI.correction_factor = get(handles.corr_factor_checkbox, 'Value');
for l=1:length(handles.DATA.users.spec_users.e_stor)
    len = length(handles.DATA.users.spec_users.e_stor(l).profile.data);
    set(handles.(genvarname(['cat' num2str(l) '_ltext'])), 'string', sec2timestr(len));
end
% category text update
temp_sum = 0;
len=length(handles.DATA.categories.str);
for i=1:len
    % get used categories from checkboxes
    handles.GUI.categories.checkbox(i) = get(handles.(genvarname(['cat' num2str(i) '_checkbox'])), 'Value');
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end
% prpability text update
for j=1:length(handles.DATA.categories.prob)
    if(handles.GUI.categories.checkbox(j))
        handles.DATA.GUIdata.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
    else
        handles.DATA.GUIdata.categories.num(j) = 0;
    end
end
% load text update
for k=1:len
    set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.GUIdata.categories.num(k)*100, '%7.2f'), ' %']);
    set(handles.(genvarname(['cat' num2str(k) '_static_text'])), 'string', ['(' num2str(handles.DATA.categories.prob(k)*100, '%7.2f'), '%)']);
    set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k), '%7.1f'), 'Wh']);
end

% pre-calculates vdew probability distribution
handles = premeasure_vdewdistribution(handles);
%get current date for the first house
cur_date = floor(datenum(now)); 
handles.DATA.simulation_period(1).date = cur_date;
dat(1,1) = {datestr(cur_date, 'yyyy-mm-dd')};
% get the load profile number for the current date
[a, b, handles.DATA.simulation_period(1).profile_num] = SLP_number(handles.DATA.simulation_period(1).date);
dat(1,2)={a};
dat(1,3)={b};
% update the font if the current date is a holiday
if(~isempty(strfind(b, 'H)')))
    if(~isempty(strfind(b, '(s')))
        dat(1,1:2)= strcat(...
        '<html><span style="color: #009900">', ...
        dat(1,1:2), ...
        '</span></html>');
        dat(1,3)= strcat(...
        '<html><span style="color: #009900; font-weight: bold;">', ...
        dat(1,3), ...
        '</span></html>');
    else
        dat(1,1:2)= strcat(...
        '<html><span style="color: #CC0000">', ...
        dat(1,1:2), ...
        '</span></html>');
        dat(1,3)= strcat(...
        '<html><span style="color: #CC0000; font-weight: bold;">', ...
        dat(1,3), ...
        '</span></html>');
    end
end
% calculate the energy for the current date
handles.DATA.simulation_period(1).energy = handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(datestr(cur_date,'yyyy'))))*(1 + handles.GUI.correction_factor*(vdew_factor(floor(datenum(now)))-1));
dat(1,4) = {[num2str(handles.DATA.simulation_period(1).energy, '%7.1f'), ' Wh']};
% write the gathered information to the GUI table
set(handles.profile_type_table, 'data', dat)
% update the GUI text with information about the current date
cur_vec = datevec(cur_date);
% year
set(handles.from_year_edit,'String', num2str(cur_vec(1)));
set(handles.to_year_edit,'String', num2str(cur_vec(1)));
% month
set(handles.from_month_edit,'String', num2str(cur_vec(2)));
set(handles.to_month_edit,'String', num2str(cur_vec(2)));
% day
set(handles.from_day_edit,'String', num2str(cur_vec(3)));
set(handles.to_day_edit,'String', num2str(cur_vec(3)));

% DATA:
% create a first house with standard data
handles.first = house(0, handles.DATA.GUIdata, handles.DATA.simulation_period, handles.DATA.users);
handles.first = handles.first.standby_power_event(1);
handles.first = handles.first.cooling_events(1);
handles.first = handles.first.continuous_events(1);
%handles.first = handles.first.spec_events(); no measured devices
handles.first = handles.first.insert_events(vdew_rand(handles.DATA.vdew(10), handles.DATA.GUIdata.scale_input, (handles.first.event_count()+20)), 1);

% GUI: plot first results
% plot output profile with the standard house data
bar(handles.output_axes, handles.first.days(1).loadcurve.active);
bar(handles.single_output_axes, handles.first.days(1).loadcurve.active);
% modify plots for right axes assignment
mod_plot(handles.output_axes, length(handles.first.days(1).loadcurve.active), 'load[W]');
mod_plot(handles.single_output_axes, length(handles.first.days(1).loadcurve.active), 'load[W]');
% update the energy text field with the current house energy consumption
set(handles.norm_load_text, 'string', [num2str((sum(handles.first.days(1).loadcurve.active)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
set(handles.sing_load_text, 'string', [num2str((sum(handles.first.days(1).loadcurve.active)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);

% default command line output for generator
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = generator_OutputFcn(hObject, eventdata, handles) 
% no interaction with the command line, except error handling
varargout{1} = handles.output;

%% Step1 - Power demand input, VDEW Selection based on current timespan, Timescale 
% 1.1 Input: yearly consumption
function yearly_load_edit_Callback(hObject, eventdata, handles)
cur_load = str2double(get(handles.yearly_load_edit,'String'));
if(isnan(cur_load))
    cur_load = 1000; %input in kWh
    set(handles.yearly_load_text, 'string', 'NaN, used 1000 kWh(std)');
else
    set(handles.yearly_load_text, 'string', [num2str(cur_load), ' kWh']);
end
handles.DATA.GUIdata.usage_data = 1000*cur_load; % kWh --> Wh
set(handles.calc_cat_text, 'string', 'parameter changed');
guidata(hObject,handles);

% 1.2 Popupmenu selection of used time step size
function timescale_popupmenu_Callback(hObject, eventdata, handles)
val=get(hObject,'Value');
str=get(hObject,'String');
switch str{val}
    case '1 sec'
        handles.DATA.GUIdata.scale_input = 1;
    case '2 sec'
        handles.DATA.GUIdata.scale_input = 2;
    case '5 sec'
        handles.DATA.GUIdata.scale_input = 5;
    case '10 sec'
        handles.DATA.GUIdata.scale_input = 10;
    case '15 sec'
        handles.DATA.GUIdata.scale_input = 15;
    case '20 sec'
        handles.DATA.GUIdata.scale_input = 20;
    case '30 sec'
        handles.DATA.GUIdata.scale_input = 30;
    case '60 sec'
        handles.DATA.GUIdata.scale_input = 60;
end
handles.DATA.GUIdata.scale_used = 24/(handles.DATA.GUIdata.scale_input/(60*60));
% Update handles structure
guidata(hObject,handles);

% 1.3 Select SLP's based on simulation range
function SLP_selection_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles);% set GUI to busy state
% get range from GUI
tmp_from(1) = str2double(get(handles.from_year_edit,'String'));
tmp_from(2) = str2double(get(handles.from_month_edit,'String'));
tmp_from(3) = str2double(get(handles.from_day_edit,'String'));
tmp_from_datenum = datenum(tmp_from); % save beginning of simulation range to a var

tmp_to(1) = str2double(get(handles.to_year_edit,'String'));
tmp_to(2) = str2double(get(handles.to_month_edit,'String'));
tmp_to(3) = str2double(get(handles.to_day_edit,'String'));
tmp_to_datenum = datenum(tmp_to); % save end of simulation range to a var

runvar=1;
% variable for SLP information table is initialized
dat={zeros(tmp_to_datenum-tmp_from_datenum,3)};
% update the font if the current date is a holiday
for i=tmp_from_datenum:tmp_to_datenum
	handles.DATA.simulation_period(runvar).date = tmp_from_datenum+runvar-1;
    dat(runvar,1) = {datestr(tmp_from_datenum+runvar-1, 'yyyy-mm-dd')};
    %calculate SLP for current date
    [a, b, handles.DATA.simulation_period(runvar).profile_num] = SLP_number(tmp_from_datenum+runvar-1);
    dat(runvar,2)={a};
    dat(runvar,3)={b};
    if(~isempty(strfind(b, 'H)')))
        if(~isempty(strfind(b, '(s')))
            dat(runvar,1:2)= strcat(...
            '<html><span style="color: #009900">', ...
            dat(runvar,1:2), ...
            '</span></html>');
            dat(runvar,3)= strcat(...
            '<html><span style="color: #009900; font-weight: bold;">', ...
            dat(runvar,3), ...
            '</span></html>');
        else
            dat(runvar,1:2)= strcat(...
            '<html><span style="color: #CC0000">', ...
            dat(runvar,1:2), ...
            '</span></html>');
            dat(runvar,3)= strcat(...
            '<html><span style="color: #CC0000; font-weight: bold;">', ...
            dat(runvar,3), ...
            '</span></html>');
        end
    end
    % calculate the energy for the current date
    handles.DATA.simulation_period(runvar).energy = handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(datestr(tmp_from_datenum+runvar-1,'yyyy'))))*(1 + handles.GUI.correction_factor*(vdew_factor(tmp_from_datenum+runvar-1)-1));
    dat(runvar,4) = {[num2str(handles.DATA.simulation_period(runvar).energy, '%7.1f'), ' Wh']};
    runvar=runvar+1;
end
% write the gathered information to the GUI table
set(handles.profile_type_table, 'data', dat)
busy_switch(handles); % set GUI to not busy state
guidata(hObject,handles);
% 1.3.1 SLP correction factor
function corr_factor_checkbox_Callback(hObject, eventdata, handles)
handles.GUI.correction_factor = get(handles.corr_factor_checkbox, 'Value'); % boolean variable
guidata(hObject,handles);
function day_correction_factor = vdew_factor(date_num) %correction factor based on the current day of the year
year = datevec(date_num);
fday = datenum([year(1) 01 01 0 0 0]);
days = datenum(date_num)-fday+1;
a=-3.92*10^-10;
b=3.2*10^-7;
c=-7.02*10^-5;
d=2.1*10^-3;
e=1.24;
day_correction_factor =a*(days^4)+b*(days^3)+c*(days^2)+d*(days^1)+e; %correction factor based on the current day of the year
% 1.3.2 Calculate SLP (season and weekday)
function [season, day, profile_number] = SLP_number(day_vec)
%1. season selection; season=[sunday workday saturday]
day_vec = datevec(day_vec);
winter = [3 4 2];
interim = [9 10 8];
summer = [6 7 5];

switch day_vec(2)
    case {1,2,11,12} %winter
        tmp_number = winter;
        season = 'Winter';
    case 3
        if(day_vec(3)<=20) %winter
            tmp_number = winter;
            season = 'Winter';
        else %interim
            tmp_number = interim;
            season = 'Interim Period';
        end
    case {4,10} %interim
        tmp_number = interim;
        season = 'Interim Period';
    case 5
        if(day_vec(3)<=14) %interim
            tmp_number = interim;
            season = 'Interim Period';
        else %summer
            tmp_number = summer;
            season = 'Summer';
        end
    case {6,7,8} %summer
        tmp_number = summer;
        season = 'Summer';
    case 9
        if(day_vec(3)<=14) %summer
            tmp_number = summer;
            season = 'Summer';
        else %interim
            tmp_number = interim;
            season = 'Interim Period';
        end
end
%2. holiday check
%2.1 easter calculation
a = mod(day_vec(1),19);
b = mod(day_vec(1),4);
c = mod(day_vec(1),7);
d = mod((19*a + 24), 30);
e = mod((2*b + 4*c + 6*d + 5), 7);   
easter_day = (22 + d + e);
if(easter_day/31>1)
    easter_day=easter_day-31;
    easter = datenum([day_vec(1) 04 easter_day 0 0 0]);
else
    easter = datenum([day_vec(1) 03 easter_day 0 0 0]);
end
%2.2 moveable holidays - easter related
holidays(2) = easter-2;
sholidays(1) = easter-1;
holidays(3) = easter;
holidays(4) = easter+1;
holidays(5) = easter+39;
holidays(6) = easter+50;
%holidays(7) = easter+60;
%2.3 fixed holidays
holidays(1) = datenum([day_vec(1) 01 01 0 0 0]); %new year
holidays(8) = datenum([day_vec(1) 05 01 0 0 0]); %labour day
holidays(9) = datenum([day_vec(1) 10 03 0 0 0]); %german unity
holidays(10) = datenum([day_vec(1) 11 01 0 0 0]); %allhallows
holidays(11) = datenum([day_vec(1) 12 25 0 0 0]); %first day of chrismas
holidays(12) = datenum([day_vec(1) 12 26 0 0 0]); %second day of chrismas
sholidays(2) = datenum([day_vec(1) 12 24 0 0 0]); %christmas eve
sholidays(3) = datenum([day_vec(1) 12 31 0 0 0]); %new year's eve
% change day string if current day is a holiday
if(~isempty(find(holidays-datenum(day_vec)==0)))
    profile_number=tmp_number(1);
    day = [datestr(day_vec, 'dddd') ' (H)'];
elseif(~isempty(find(sholidays-datenum(day_vec)==0)))
    profile_number=tmp_number(3);
    day = [datestr(day_vec, 'dddd') ' (sH)'];
    if(strcmp(datestr(day_vec, 'dddd'), 'Sunday'))
        profile_number=tmp_number(1);
        day = [datestr(day_vec, 'dddd') ' (H)'];
    end
else
%3. day selection
    day_str = datestr(day_vec, 'dddd');
    if(strcmp(day_str, 'Sunday'))
        profile_number = tmp_number(1);
        day = datestr(day_vec, 'dddd');
    elseif(strcmp(day_str, 'Saturday'))
        profile_number = tmp_number(3);
        day = datestr(day_vec, 'dddd');
    else
        profile_number = tmp_number(2);
        day = datestr(day_vec, 'dddd');
    end    
end
function ly = leapyear(year) %leapyear check
ly = 0;
if year > 1582
	if mod(year, 4) == 0
        ly = 1;
    end
	if mod(year, 100) == 0
        ly = 0;
    end
	if mod(year, 400) == 0
        ly = 1;
    end
end


%% Step2 - Category selection and calculation
% execute after applying all inputs for category settings by users command
function calc_cat_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles);
temp_sum = 0;
for i=1:length(handles.GUI.categories.checkbox)
    if(handles.GUI.categories.checkbox(i))
        temp_sum=temp_sum+handles.DATA.categories.prob(i);
    end
end
if(temp_sum == 0)
    set(handles.calc_cat_text, 'string', '0 - failed');
else
    for j=1:length(handles.DATA.categories.prob)
        if(handles.GUI.categories.checkbox(j))
            handles.DATA.GUIdata.categories.num(j) = handles.DATA.categories.prob(j)/temp_sum;
        else
            handles.DATA.GUIdata.categories.num(j) = 0;
        end
    end
    handles = premeasure_vdewdistribution(handles);
    % cat load text-field update
    for k=1:length(handles.DATA.categories.prob)
        if(strcmp(get(handles.(genvarname(['cat' num2str(k) '_axes'])), 'Visible'), 'on'))
            handles.DATA.users.spec_users.e_stor(k).to_use=1;
            % cycle refresh
            % usage_factor = spec_energy/(sum(obj.users.spec_users.e_stor(i).profile.data.active)/3600);
            % obj.days(day).usage_factor(i) = usage_factor;
            
            %HINT: Aparent power (S = P+j*Q)
            active_power = sum(handles.DATA.users.spec_users.e_stor(k).profile.data.active)/3600;
            reactive_power = sum(handles.DATA.users.spec_users.e_stor(k).profile.data.reactive)/3600;
            aparent_power_abs = abs(complex(active_power,reactive_power));
            usage_factor=(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k))/aparent_power_abs;
            set(handles.(genvarname(['cat' num2str(k) '_consumtext'])), 'String', [num2str(aparent_power_abs/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
        end
        set(handles.(genvarname(['cat' num2str(k) '_load_text'])), 'string', [num2str(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(k), '%7.1f'), 'Wh']);
        set(handles.(genvarname(['cat' num2str(k) '_text'])), 'string', [num2str(handles.DATA.GUIdata.categories.num(k)*100, '%7.2f'), ' %']);
    end
    set(handles.calc_cat_text, 'string', 'calc done');
end
busy_switch(handles);
guidata(hObject,handles);
% modify SLP matrices to a propability density
function handles = premeasure_vdewdistribution(handles)
% overwrites used data for event spreading, subtracts percentage for standby 
handles.DATA.mSLP.data = handles.DATA.SLP.prob-(handles.DATA.GUIdata.categories.num(1)+handles.DATA.GUIdata.categories.num(10))/96;
% if prob<0 --> prob=0
handles.DATA.mSLP.data(find(handles.DATA.mSLP.data<0))=0;
% recalculate, so sum is 1 (propability density)
vdew_reduced_sum=sum(handles.DATA.mSLP.data);
for i=1:length(vdew_reduced_sum)
handles.DATA.mSLP.data(:,i)=handles.DATA.mSLP.data(:,i)/vdew_reduced_sum(i);
end
% help matrix initialization
len=10000;
for j=1:length(vdew_reduced_sum)
    handles.DATA.vdew(j).interval=zeros(len,1);
    handles.DATA.vdew(j).rescaled=round(handles.DATA.mSLP.data(:,j)*len);
    handles.DATA.vdew(j).startpos=zeros(length(handles.DATA.vdew(j).rescaled),1);
end
% generating help matrix
for j=1:length(vdew_reduced_sum)
    handles.DATA.vdew(j).startpos(1)=1;
    handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(1):handles.DATA.vdew(j).rescaled(1))=1;
    for i=2:length(handles.DATA.mSLP.data)
       handles.DATA.vdew(j).startpos(i)=handles.DATA.vdew(j).startpos(i-1)+handles.DATA.vdew(j).rescaled(i-1);
       handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(i):(handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i)))=i;
    end
    % error catching
    if((handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i))<length(handles.DATA.vdew(j).interval))
        handles.DATA.vdew(j).interval(handles.DATA.vdew(j).startpos(i)+handles.DATA.vdew(j).rescaled(i)+1:length(handles.DATA.vdew(j).interval))=i;
    end
end


%% Step3 - Main function for simulation 
% Executes loop for #houses
function plot_complete_pushbutton_Callback(hObject, eventdata, handles)
busy_switch(handles); % GUI state: busy
handles.no_houses = str2double(get(handles.input_houses_edit,'String'));
handles.DATA.complete_loadcurve.active = zeros(length(handles.DATA.simulation_period), handles.DATA.GUIdata.scale_used);
handles.DATA.complete_loadcurve.reactive = zeros(length(handles.DATA.simulation_period), handles.DATA.GUIdata.scale_used);
% hide multiple day selection
set(handles.day_num_popup, 'Visible', 'off');
set(handles.single_datum_text, 'Visible', 'off');
set(handles.day_num_norm_popup, 'Visible', 'off');
set(handles.norm_datum_text, 'Visible', 'off');
set(handles.single_day_text, 'Visible', 'off');
set(handles.norm_day_text, 'Visible', 'off');
set(handles.no_days_text, 'Visible', 'off');
% error-catching
if(isnan(handles.no_houses))
    handles.no_houses = 1;
end
% show stop button
set(handles.stop_pushbutton, 'Visible', 'on');
set(handles.stop_text, 'Visible', 'off');

h_run_var = 0;
handles.stopprocess = 0;
% initialize houses
for i=1:handles.no_houses
    handles.stor(i) = house(i, handles.DATA.GUIdata, handles.DATA.simulation_period, handles.DATA.users);
end
tic % time counter start tag
d_len=length(handles.DATA.simulation_period);
h_len=handles.no_houses;
for j=1:length(handles.DATA.simulation_period) % loop for days
    set(handles.days_progress_text, 'string', ['Day ' num2str(j) '/' num2str(d_len) ':']);
    for i = 1:handles.no_houses % loop for houses
        if(strcmp(get(handles.stop_text, 'String'), 'STOP'))
            break;
        end
        h_run_var = h_run_var + 1;
        % process house no 'i' at day 'j'
        handles.stor(i) = handles.stor(i).standby_power_event(j);
        if(handles.GUI.categories.checkbox(1))
            handles.stor(i) = handles.stor(i).cooling_events(j);
        end
        handles.stor(i) = handles.stor(i).spec_events(j);
        handles.stor(i) = handles.stor(i).continuous_events(j);
        handles.stor(i) = handles.stor(i).insert_events(vdew_rand(handles.DATA.vdew(handles.DATA.simulation_period(j).profile_num), handles.DATA.GUIdata.scale_input, handles.stor(i).event_count()+40), j);
        if(length(handles.DATA.simulation_period)>1)
            handles.stor(i) = handles.stor(i).next_day(j); % clears standard eventlist
        end
        % generate complete load curve
        handles.DATA.complete_loadcurve.active(j,:)=handles.DATA.complete_loadcurve.active(j,:)+transpose(handles.stor(i).days(j).loadcurve.active);
        handles.DATA.complete_loadcurve.reactive(j,:)=handles.DATA.complete_loadcurve.reactive(j,:)+transpose(handles.stor(i).days(j).loadcurve.reactive);
        % progressbar update
        uiprogressbar(i, handles.no_houses, handles, 'days');
        if(d_len==1)
            uiprogressbar(i, handles.no_houses, handles, 'total');
        end
    end
    uiprogressbar(j, length(handles.DATA.simulation_period), handles, 'total');
    if(h_len==1)
        uiprogressbar(j, length(handles.DATA.simulation_period), handles, 'days');
    end
end
% text update
if(h_run_var==handles.no_houses)
    set(handles.stop_text, 'string', 'finished');
else
    set(handles.stop_text, 'string', 'stopped');
end
set(handles.stop_pushbutton, 'Visible', 'off');
set(handles.stop_text, 'Visible', 'on');
if (h_run_var == 0)
    h_run_var = 1;
end
% set xls export range to maximum
set(handles.xls_export1_edit, 'String', num2str(1));
set(handles.xls_export2_edit, 'String', num2str(length(handles.stor)));
set(handles.xls_export1_days_edit,'String', num2str(1));
set(handles.xls_export2_days_edit,'String', num2str(length(handles.stor(1).days)));
% text field updates with results
set(handles.calc_cat_text, 'string', '');
set(handles.no_houses_text, 'string', ['/', num2str(length(handles.stor))]);
set(handles.norm_load_text, 'string', [num2str((sum(handles.DATA.complete_loadcurve.active(1,:))/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);

if(j>1)
    % single plot refresh, multiple days
    set(handles.day_num_popup, 'Visible', 'on');
    set(handles.single_datum_text, 'Visible', 'on');
    set(handles.day_num_norm_popup, 'Visible', 'on');
    set(handles.norm_datum_text, 'Visible', 'on');
    set(handles.single_day_text, 'Visible', 'on');
    set(handles.norm_day_text, 'Visible', 'on');
    set(handles.no_days_text, 'Visible', 'on');
    set(handles.no_days_text, 'string', ['/' num2str(length(handles.DATA.simulation_period))]);
    for m=1:j
        day_text(m) = {['Day',num2str(m)]};
    end
    % dropdown menu for multiple days
    set(handles.day_num_popup, 'String', day_text);
    set(handles.day_num_norm_popup, 'String', day_text);
    set(handles.day_num_norm_popup, 'value', 1);
    set(handles.day_num_norm_popup, 'value', 1);
else
    % dropdown menu for multiple days
    set(handles.day_num_popup, 'Visible', 'off');
    set(handles.single_datum_text, 'Visible', 'off');
    set(handles.day_num_norm_popup, 'Visible', 'off');
    set(handles.norm_datum_text, 'Visible', 'off');
    set(handles.single_day_text, 'Visible', 'off');
    set(handles.norm_day_text, 'Visible', 'off');
    set(handles.no_days_text, 'Visible', 'off');
end

% single plot refresh, multiple households  
for n=1:length(handles.stor)
    house_text(n) = {['House',num2str(n)]};
end
set(handles.houseno_popup, 'String', house_text);
set(handles.houseno_popup, 'value', 1);
% handles.single_day_text, handles.norm_day_text if holyday --> text *
cur_table = get(handles.profile_type_table,'data');
if(~isempty(strfind(cur_table{1,3},'H)')))
    set(handles.single_day_text, 'string', [datestr(handles.stor(1).days(1).date, 'dddd') '*']);
    set(handles.norm_day_text, 'string', [datestr(handles.stor(1).days(1).date, 'dddd') '*']);
else
    set(handles.single_day_text, 'string', datestr(handles.stor(1).days(1).date, 'dddd'));
    set(handles.norm_day_text, 'string', datestr(handles.stor(1).days(1).date, 'dddd'));
end
% plot resulting load profiles
bar(handles.single_output_axes, handles.stor(1).days(1).loadcurve.active);
handles = mod_single_plot(handles, 1, 1);
bar(handles.output_axes, handles.DATA.complete_loadcurve.active(1,:));
mod_plot(handles.output_axes, length(handles.DATA.complete_loadcurve.active(1,:)), 'load[W]');
set(handles.norm_datum_text, 'string', datestr(handles.stor(1).days(1).date, 'yyyy-mm-dd'));
busy_switch(handles);% GUI state: not busy

guidata(hObject,handles);

% returns a list of random numbers based on vdew propability distribution
function startpos_list = vdew_rand(vdew, scale_input, count)
startpos_list=zeros(count,1);
for i=1:count
    pos_y = floor(rand*length(vdew.interval))+1;
    pos_x = vdew.interval(pos_y);
    frac = (pos_y-vdew.startpos(pos_x))/vdew.rescaled(pos_x);
    startpos_list(i) = floor((pos_x-1+frac)*15*60/scale_input)+1;
end

% Callback for stop button, dialog for confimation of the cancel command
function stop_pushbutton_Callback(hObject, eventdata, handles)
selection = questdlg('Do you want to stop this process?',...
                     'Stop process',...
                     'Yes','No','Yes');
switch selection,
   case 'Yes',
         set(handles.stop_text, 'String', 'STOP');
         drawnow;
   case 'No'
         return
end


%% Step4 - functions to save results
% saves handles.stor structure to .mat file
function save_data_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('houses_v','Saving house data to...');
if pathname == 0 %if the user pressed cancelled, then exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname, filename); 
busy_switch(handles);
%saves houses with all information
%save(saveDataName, '-struct', 'handles', 'stor');
save(saveDataName, 'handles');
busy_switch(handles);
% saves loadprofiles to .mat file
function save_profiles_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('loadprofiles_v','Saving house data to...');
if pathname == 0 %if the user pressed cancelled, then exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname, filename); 
hand_temp = waitbar(0,'Exporting profiles...');
busy_switch(handles);
% create a matrix which only contains load profiles
h_len=length(handles.stor);
d_len=length(handles.stor(1).days);
l_len=length(handles.stor(1).days(1).loadcurve.active);
loadprofiles=zeros(d_len,h_len);
for i=1:d_len
        for j=1:h_len
        loadprofiles(((i-1)*l_len+i):(i*l_len+i-1), j) = handles.stor(j).days(i).loadcurve.active(1:l_len);
        waitbar((i-1)/d_len+j/(d_len*h_len));
        end
    waitbar(i/d_len);
end
close(hand_temp);
hand_temp = waitbar(1,'Writing to disk...');
save(saveDataName, 'loadprofiles');
close(hand_temp);
busy_switch(handles);
% saves a calculation to a .fig file
function save_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the settings file
[filename,pathname] = uiputfile('guidata_v','Saving GUI settings to...');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveDataName = fullfile(pathname,filename); 
busy_switch(handles);
%saves the gui data
hgsave(saveDataName);
busy_switch(handles);
% loads a saved calculation and closes the old
function load_gui_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to choose which settings to load
[filename, pathname] = uigetfile('*.fig', 'Loading GUI settings file...');

%construct the path name of the file to be loaded
loadDataName = fullfile(pathname,filename);

%this is the gui that will be closed once we load the new settings
theCurrentGUI = gcf;  

%load the settings, which creates a new gui
hgload(loadDataName); 

%closes the old gui
close(theCurrentGUI);
% XLS export function
function xls_export_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the ,file
[filename,pathname] = uiputfile('*.xlsx','Saving House Data to xls...','loaddata_v');
if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
%construct the path name of the save location
saveName = fullfile(pathname,filename);
busy_switch(handles);
excel_export = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

% XLS EXPORT STARTS -->
if(isfield(handles, 'stor'))
    hand_temp = waitbar(0,'Exporting days to EXCEL file...');
    pro_len = length(handles.DATA.complete_loadcurve.active);
    house_len=length(handles.stor);
    day_len=length(handles.DATA.simulation_period);
    cur_day=1;
    % time string generation for summation of all houses for a day
    for i=1:pro_len
        time.temp{i,1} = datestr(handles.DATA.simulation_period(cur_day).date+(i-1)/pro_len, 'HH:MM:SS');
    end
    xlswrite(saveName, {[num2str(house_len) ' HS/day']}, 'comp_load_active', 'A1:A1');
    xlswrite(saveName, time.temp, 'comp_load_active', 'A2');
    xlswrite(saveName, {[num2str(house_len) ' HS/day']}, 'comp_load_reactive', 'A1:A1');
    xlswrite(saveName, time.temp, 'comp_load_reactive', 'A2');
    
    for i=1:day_len
        date.temp{1,i}=datestr(handles.DATA.simulation_period(i).date, 'yyyy-mm-dd');
    end
    xlswrite(saveName, date.temp, 'comp_load_active', 'B1');
    xlswrite(saveName, transpose(handles.DATA.complete_loadcurve.active), 'comp_load_active', 'B2');
    xlswrite(saveName, date.temp, 'comp_load_reactive', 'B1');
    xlswrite(saveName, transpose(handles.DATA.complete_loadcurve.reactive), 'comp_load_reactive', 'B2');
    
    close(hand_temp);
    hand_temp = waitbar(0,'Exporting houses to EXCEL file...');
    % write households
    % error catching - EXCEL cell address range
    h_startp = str2double(get(handles.xls_export1_edit,'String'));
    h_endp = str2double(get(handles.xls_export2_edit,'String'));
    d_startp = str2double(get(handles.xls_export1_days_edit,'String'));
    d_endp = str2double(get(handles.xls_export2_days_edit,'String'));
    if(pro_len>1440 && d_endp-d_startp>12)
        h.temp = errordlg('Setting Day Export Range to 12!','EXCEL Adress Range exceeded');
        d_startp = 1;
        d_endp = 12;
    end
    if(h_endp-h_startp+1>675)
        h.temp = errordlg('Setting House Export Range to 675!','EXCEL Adress Range exceeded');
        h_startp = 1;
        h_endp = 675;
    end
    if(pro_len==1440 && d_endp-d_startp>728)
        h.temp = errordlg('Setting Day Export Range to 728!','EXCEL Adress Range exceeded');
        d_startp = 1;
        d_endp = 728;
    end
    %first row
    text.temp{1,1}='Time';
    for i=1:(h_endp-h_startp+1)
        text.temp{1,i+1}=['House' num2str(h_startp+i-1)];
    end
    xlswrite(saveName, text.temp, 'houses_active', 'A1');
    xlswrite(saveName, text.temp, 'houses_reactive', 'A1');
    %ACTIVE POWER
    for cur_day=1:(d_endp-d_startp+1)
        % time string generation
        for t=1:pro_len
            time.temp{t,1} = datestr(handles.DATA.simulation_period(cur_day+d_startp-1).date+(t-1)/pro_len, 'yyyy-mm-dd HH:MM:SS');
        end
        xlswrite(saveName, time.temp, 'houses_active', ['A' num2str(((cur_day-1)*pro_len+1)+1) ':A' num2str(cur_day*pro_len+1)]);
        for j=2:((h_endp-h_startp+1)+1)
            if(j<=26)
                export_col=excel_export(j);
            else
                export_col=char({[excel_export(floor(j/26)) excel_export(mod(j,26))]});
            end
            xlswrite(saveName, handles.stor(j+h_startp-1-1).days(cur_day+d_startp-1).loadcurve.active, 'houses_active', [export_col num2str(((cur_day-1)*pro_len+1)+1) ':' export_col num2str(cur_day*pro_len+1)]);
            waitbar((cur_day-1)/(d_endp-d_startp+1)+(j-1)/((d_endp-d_startp+1)*(h_endp-h_startp+1)));
        end
        waitbar(cur_day/(d_endp-d_startp+1));
    end
    %REACTIVE POWER
    for cur_day=1:(d_endp-d_startp+1)
        % time string generation
        for t=1:pro_len
            time.temp{t,1} = datestr(handles.DATA.simulation_period(cur_day+d_startp-1).date+(t-1)/pro_len, 'yyyy-mm-dd HH:MM:SS');
        end
        xlswrite(saveName, time.temp, 'houses_reactive', ['A' num2str(((cur_day-1)*pro_len+1)+1) ':A' num2str(cur_day*pro_len+1)]);
        for j=2:((h_endp-h_startp+1)+1)
            if(j<=26)
                export_col=excel_export(j);
            else
                export_col=char({[excel_export(floor(j/26)) excel_export(mod(j,26))]});
            end
            xlswrite(saveName, handles.stor(j+h_startp-1-1).days(cur_day+d_startp-1).loadcurve.reactive, 'houses_reactive', [export_col num2str(((cur_day-1)*pro_len+1)+1) ':' export_col num2str(cur_day*pro_len+1)]);
            waitbar((cur_day-1)/(d_endp-d_startp+1)+(j-1)/((d_endp-d_startp+1)*(h_endp-h_startp+1)));
        end
        waitbar(cur_day/(d_endp-d_startp+1));
    end
    close(hand_temp);
else
    xlswrite(saveName, handles.first.days(1).loadcurve.active, ['house_' 'first_active']);
    xlswrite(saveName, handles.first.days(1).loadcurve.reactive, ['house_' 'first_reactive']);
end
% XLS EXPORT ENDS <--

busy_switch(handles);
% TXT export function
function save_data2txt_pushbutton_Callback(hObject, eventdata, handles)
%allows the user to specify where to save the file
[filename,pathname] = uiputfile('*.txt','Saving data to text file...','load_v');

if pathname == 0 %if the user pressed cancelled, then we exit this callback
    return
end
busy_switch(handles);
%construct the path name of the save location
saveName = fullfile(pathname,filename); 
sN_active = [saveName(1:length(saveName)-4) '_active.txt'];
sN_reactive = [saveName(1:length(saveName)-4) '_reactive.txt'];
hand_temp = waitbar(0,'Exporting ACTIVE POWER to txt...');
% TEXT EXPORT ACTIVE POWER STARTS -->
f_ID = fopen(sN_active,'w');
l_houses=length(handles.stor);
%create the first line
fprintf(f_ID,'%-19s','Time');
for i=1:l_houses
    cur_house = ['House' num2str(i)];
    fprintf(f_ID,'\t%-8s', cur_house);
end
fprintf(f_ID,'\n');
d_len=length(handles.stor(1).days);
pro_len=length(handles.stor(1).days(1).loadcurve.active);
for t=1:d_len
    for i=1:pro_len
        time_temp{i,1} = datestr(handles.stor(1).days(t).date+(i-1)/pro_len, 'yyyy-mm-dd HH:MM:SS');
    end
    for z=1:pro_len
        fprintf(f_ID,'%-19s',time_temp{z,1});
        for i=1:l_houses
            fprintf(f_ID,'\t%-8.2f', handles.stor(i).days(t).loadcurve.active(z,1)); % write element to text file
        end
        fprintf(f_ID,'\n');
        waitbar((t-1)/d_len+z/(d_len*pro_len));
    end
    waitbar(t/d_len);
end
fclose(f_ID);
close(hand_temp);
% TEXT EXPORT ACTIVE POWER ENDS <--
hand_temp = waitbar(0,'Exporting REACTIVE POWER to txt...');
% TEXT EXPORT REACTIVE POWER STARTS -->
f_ID = fopen(sN_reactive,'w');
l_houses=length(handles.stor);
%create the first line
fprintf(f_ID,'%-19s','Time');
for i=1:l_houses
    cur_house = ['House' num2str(i)];
    fprintf(f_ID,'\t%-8s', cur_house);
end
fprintf(f_ID,'\n');
d_len=length(handles.stor(1).days);
pro_len=length(handles.stor(1).days(1).loadcurve.reactive);
for t=1:d_len
    for i=1:pro_len
        time_temp{i,1} = datestr(handles.stor(1).days(t).date+(i-1)/pro_len, 'yyyy-mm-dd HH:MM:SS');
    end
    for z=1:pro_len
        fprintf(f_ID,'%-19s',time_temp{z,1});
        for i=1:l_houses
            fprintf(f_ID,'\t%-8.2f', handles.stor(i).days(t).loadcurve.reactive(z,1)); % write element to text file
        end
        fprintf(f_ID,'\n');
        waitbar((t-1)/d_len+z/(d_len*pro_len));
    end
    waitbar(t/d_len);
end
fclose(f_ID);
close(hand_temp);
% TEXT EXPORT REACTIVE POWER ENDS <--
busy_switch(handles);
guidata(hObject,handles);


%% helpfunctions for GUI apperance
% GUI busy text visibility
function busy_switch(handles)
status = get(handles.guibusy_text, 'Visible');
if(strcmp(status,'off')) 
    set(gcf, 'Pointer', 'watch'); % busy
    set(handles.guibusy_text, 'Visible', 'on');
else
    set(gcf, 'Pointer', 'arrow'); % not busy
    set(handles.guibusy_text, 'Visible', 'off');
end
drawnow;
% Callback function for single plot update by dropdownmenu selection
function houseno_popup_Callback(hObject, eventdata, handles)
d_pos=get(handles.day_num_popup,'Value');
h_pos=get(hObject,'Value');
% plot selected house
bar(handles.single_output_axes, handles.stor(h_pos).days(d_pos).loadcurve.active);
if(isfield(handles, 'stor')) 
    handles = mod_single_plot(handles, h_pos, d_pos);
end
guidata(hObject,handles);
% helpfunction for modification of single household plot
function handles = mod_single_plot(handles, pos, day)
runvar=1;
hold on;
for m=2:length(handles.stor(pos).spec_estor)
    if(handles.stor(pos).spec_estor(m).profile.cycles(day)>0)
        for i=1:length(handles.stor(pos).spec_estor(m).startpos(day).num)
            rndclr=[rand, rand, rand];
            if(handles.stor(pos).spec_estor(m).startpos(day).num(i)+handles.stor(pos).spec_estor(m).profile.duration>handles.stor(pos).GUIdata.scale_used)
                plot([handles.stor(pos).spec_estor(m).profile.data.active(handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).startpos(day).num(i)+1:handles.stor(pos).spec_estor(m).profile.duration); zeros((handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).profile.duration), 1); handles.stor(pos).spec_estor(m).profile.data.active(1:handles.stor(pos).GUIdata.scale_used-handles.stor(pos).spec_estor(m).startpos(day).num(i))], 'color', rndclr);
            else
                plot([zeros(handles.stor(pos).spec_estor(m).startpos(day).num(i), 1); handles.stor(pos).spec_estor(m).profile.data.active; zeros(handles.stor(pos).GUIdata.scale_used-(handles.stor(pos).spec_estor(m).startpos(day).num(i)+handles.stor(pos).spec_estor(m).profile.duration),1)], 'color', rndclr);
            end
            runvar=runvar+1;
        end
    end
end
hold off;

mod_plot(handles.single_output_axes, length(handles.stor(pos).days(day).loadcurve.active), 'load[W]');
set(handles.single_datum_text, 'string', datestr(handles.stor(pos).days(day).date, 'yyyy-mm-dd'));
set(handles.sing_load_text, 'string', [num2str((sum(handles.stor(pos).days(day).loadcurve.active)/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
% select and plot summation of houses load profiles for one day
function day_num_norm_popup_Callback(hObject, eventdata, handles)
d_pos=get(hObject,'Value');
if(isfield(handles, 'stor')) 
    bar(handles.output_axes, handles.DATA.complete_loadcurve.active(d_pos,:));
    mod_plot(handles.output_axes, length(handles.DATA.complete_loadcurve.active(d_pos,:)), 'load[W]');
    set(handles.norm_load_text, 'string', [num2str((sum(handles.DATA.complete_loadcurve.active(d_pos,:))/3600*handles.DATA.GUIdata.scale_input),'%7.1f'), ' Wh']);
    set(handles.norm_datum_text, 'string', datestr(handles.stor(1).days(d_pos).date, 'yyyy-mm-dd'));
    cur_table = get(handles.profile_type_table,'data');
    if(~isempty(strfind(cur_table{d_pos,3},'H)')))
        set(handles.norm_day_text, 'string', [datestr(handles.stor(1).days(d_pos).date, 'dddd') '*']);
    else
        set(handles.norm_day_text, 'string', datestr(handles.stor(1).days(d_pos).date, 'dddd'));
    end
end
guidata(hObject,handles);
% select and plot the selected house for a day
function day_num_popup_Callback(hObject, eventdata, handles)
d_pos=get(hObject,'Value');
h_pos=get(handles.houseno_popup,'Value');
if(isfield(handles, 'stor'))
    bar(handles.single_output_axes, handles.stor(h_pos).days(d_pos).loadcurve.active);
    handles = mod_single_plot(handles, h_pos, d_pos);
    cur_table = get(handles.profile_type_table,'data');
    if(~isempty(strfind(cur_table{d_pos,3},'H)')))
        set(handles.single_day_text, 'string', [datestr(handles.stor(1).days(d_pos).date, 'dddd') '*']);
    else
        set(handles.single_day_text, 'string', datestr(handles.stor(1).days(d_pos).date, 'dddd'));
    end
end
guidata(hObject,handles);

% function modifies a plot axes based on input parameters
function mod_plot(hand, leng, ystr)
% x-axis
set(hand,'XLim', [0 leng]);
set(hand,'XTick', [0:leng/24:leng]);
set(hand,'XTickLabel',[' 0';'  ';' 2';'  ';' 4';'  ';' 6';'  ';' 8';'  ';'10';'  ';'12';'  ';'14';'  ';'16';'  ';'18';'  ';'20';'  ';'22';'  ';'24']);
set(get(hand, 'XLabel'), 'String' ,'time[h]');
set(hand,'XGrid','on');
% y-axis
set(get(hand, 'YLabel'), 'String' ,ystr);
set(hand,'yGrid','on');

% changes visibility for categories / special event pushbuttons
function cat1_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 1, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat2_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 2, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat3_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 3, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat4_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 4, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat5_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 5, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat6_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 6, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat7_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 7, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat8_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 8, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat9_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 9, 1);
set(handles.calc_cat_text, 'string', 'parameter changed');
function cat10_checkbox_Callback(hObject, eventdata, handles)
setpushbuttonvisible(hObject, handles, 10, 0);
set(handles.calc_cat_text, 'string', 'parameter changed');

function setpushbuttonvisible(hObject, handles, nr, vis)
% updates GUI visibility
handles.GUI.categories.checkbox(nr) = get(handles.(genvarname(['cat' num2str(nr) '_checkbox'])), 'Value');
if(vis)
    status = get(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible');
    status2 = get(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible');
    if(strcmp(status, 'on') && strcmp(status2, 'off'))
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    elseif(strcmp(status2, 'on') && ~strcmp(status, 'on'))
        xbuttonupdate(hObject, handles, nr);
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'off');
    else
        set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');    
    end
end
guidata(hObject,handles);

% updates textfields and applies appearance of axes for measured divice events
function cat1_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 1);
function cat2_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 2);
function cat3_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 3);
function cat4_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 4);
function cat5_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 5);
function cat6_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 6);
function cat7_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 7);
function cat8_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 8);
function cat9_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 9);
function cat10_spec_pushbutton_Callback(hObject, eventdata, handles)
mod_spec_load(handles, 10);

function mod_spec_load(handles, num)
% updates GUI visibility
set(handles.(genvarname(['cat' num2str(num) '_spec_pushbutton'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(num) '_axes'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_x'])), 'Visible', 'on');
bar(handles.(genvarname(['cat' num2str(num) '_axes'])), handles.DATA.users.spec_users.e_stor(num).profile.data.active);
handles.DATA.users.spec_users.e_stor(num).to_use=1; %enables the event usage
leng=length(handles.DATA.users.spec_users.e_stor(num).profile.data.active);
maxi=max(handles.DATA.users.spec_users.e_stor(num).profile.data.active);
active_power = sum(handles.DATA.users.spec_users.e_stor(num).profile.data.active)/3600;
reactive_power = sum(handles.DATA.users.spec_users.e_stor(num).profile.data.reactive)/3600;
suma = abs(complex(active_power,reactive_power));
usage_factor=(handles.DATA.GUIdata.usage_data/(365+leapyear(str2double(get(handles.from_year_edit, 'String'))))*handles.DATA.GUIdata.categories.num(num))/suma;


set(handles.(genvarname(['cat' num2str(num) '_ltext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XLim', [0 leng]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTick', 0:leng:leng);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'XTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YLim', [0 (maxi*1.1)]);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTick', 0:maxi:maxi);
set(handles.(genvarname(['cat' num2str(num) '_axes'])),'YTickLabel','');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(num) '_consumtext'])), 'String', [num2str(suma/1000, '%7.1f'), 'kWh*', num2str(usage_factor, '%7.1f')]);
set(handles.calc_cat_text, 'string', 'parameter changed');

% updates the visibility of the measured devices events
function cat1_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 1);
function cat2_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 2);
function cat3_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 3);
function cat4_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 4);
function cat5_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 5);
function cat6_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 6);
function cat7_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 7);
function cat8_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 8);
function cat9_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 9);
function cat10_x_Callback(hObject, eventdata, handles)
xbuttonupdate(hObject, handles, 10);

function xbuttonupdate(hObject, handles, nr)
%updates GUI visibility
set(handles.(genvarname(['cat' num2str(nr) '_spec_pushbutton'])), 'Visible', 'on');
set(handles.(genvarname(['cat' num2str(nr) '_axes'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_x'])), 'Visible', 'off');
set(handles.(genvarname(['cat' num2str(nr) '_ltext'])), 'Visible', 'off');
cla(handles.(genvarname(['cat' num2str(nr) '_axes']))); %clears axis-objects
set(handles.(genvarname(['cat' num2str(nr) '_consumtext'])), 'Visible', 'off');
handles.DATA.users.spec_users(nr).to_use=0; %disables the event usage
set(handles.calc_cat_text, 'string', 'parameter changed');
guidata(hObject,handles);

% progressbar update function
function uiprogressbar(x, z, handles, tag)
frac = x/z;
pos = get(handles.(genvarname([tag '_progressbar_text'])),'position');
pos2 = get(handles.(genvarname([tag '_progressbar_static_text'])), 'position');
% bar position & length calculation
pos(3) = frac*(pos2(3)-2);
set(handles.(genvarname([tag '_progressbar_text'])),'position',pos,'string',sprintf('%.0f%%',frac*100))
if(strcmp(tag,'total')) % writes the remaining time to text field
    outp = sec2timestr(fix(toc*(z-x)/x));
    set(handles.progressstatus_text, 'string', [outp, ' remaining']);
end
drawnow

% this function returns string for time left - input: time left in seconds
function timestr = sec2timestr(sec)
% Convert a time measurement from seconds into a human readable string.
% Convert seconds to other units
w = floor(sec/604800); % Weeks
sec = sec - w*604800;
d = floor(sec/86400); % Days
sec = sec - d*86400;
h = floor(sec/3600); % Hours
sec = sec - h*3600;
m = floor(sec/60); % Minutes
sec = sec - m*60;
s = floor(sec); % Seconds

% Create time string
if w > 0
    if w > 9
        timestr = sprintf('%d week', w);
    else
        timestr = sprintf('%d week, %d day', w, d);
    end
elseif d > 0
    if d > 9
        timestr = sprintf('%d day', d);
    else
        timestr = sprintf('%d day, %d hr', d, h);
    end
elseif h > 0
    if h > 9
        timestr = sprintf('%d hr', h);
    else
        timestr = sprintf('%d hr, %d min', h, m);
    end
elseif m > 0
    if m > 9
        timestr = sprintf('%d min', m);
    else
        timestr = sprintf('%d min, %d sec', m, s);
    end
else
    timestr = sprintf('%d sec', s);
end


%% All 'unused' callback functions down here
% popup menu create functions 
function select_data_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function houseno_popup_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function timescale_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function day_num_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function day_num_norm_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% unused edit box create functions
function yearly_load_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function input_houses_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export1_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export2_edit_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export1_days_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xls_export2_days_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function from_year_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function from_month_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function from_day_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function to_year_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function to_month_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function to_day_edit_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% unused edit box callbacks functions
function input_houses_edit_Callback(hObject, eventdata, handles)
function xls_export1_edit_Callback(hObject, eventdata, handles)
function xls_export2_edit_Callback(hObject, eventdata, handles)
function xls_export1_days_edit_Callback(hObject, eventdata, handles)
function xls_export2_days_edit_Callback(hObject, eventdata, handles)
function from_year_edit_Callback(hObject, eventdata, handles)
function from_month_edit_Callback(hObject, eventdata, handles)
function from_day_edit_Callback(hObject, eventdata, handles)
function to_year_edit_Callback(hObject, eventdata, handles)
function to_month_edit_Callback(hObject, eventdata, handles)
function to_day_edit_Callback(hObject, eventdata, handles)