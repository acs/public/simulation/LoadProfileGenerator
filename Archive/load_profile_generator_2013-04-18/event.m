%% Event class
% Class uses constructor override to create different eventtypes
classdef event
    properties
        ID % internal identifier
        name % object name
        profile % timevariant behavior
        startpos % starting time
        to_use % special events can or cannot be used
    end
    
    methods
        function obj=event(varargin)
            switch nargin
                case 1
                    % standart event constructor only ID is given
                    obj.ID = varargin{1};
                    obj.profile.data.active = 0;
                    obj.profile.duration = 0;
                    obj.profile.energy = obj.profile.duration*obj.profile.data.active/3600;
                    obj.name = int2str(obj.ID);
                    obj.to_use=0;
                case 2
                    % timevariant behavior, ID given
                    obj.ID = varargin{1};
                    obj.profile.data.active = varargin{2};
                    obj.profile.duration=length(obj.profile.data.active);
                    obj.profile.energy = sum(obj.profile.data.active)/3600;
                    obj.name = int2str(obj.ID);
                    obj.to_use=0;
                case 3
                    % timevariant behavior and name, ID given
                    obj.ID = varargin{1};
                    obj.name = varargin{2};
                    obj.profile.data.active = varargin{3};
                    obj.profile.duration=length(obj.profile.data.active);
                    obj.profile.energy = sum(obj.profile.data.active)/3600;
                    obj.to_use=0;
                case 4
                    % ID, name, profile, duration given
                    obj.ID = varargin{1};
                    obj.name = varargin{2};
                    str_size=size(varargin{3});
                    if(str_size(2)==2)
                        obj.profile.data.active = varargin{3}(:,1);
                        obj.profile.data.reactive = varargin{3}(:,2);
                    else
                        obj.profile.data.active = varargin{3};
                    end
                    obj.profile.duration = varargin{4};
                    if(length(obj.profile.data.active)>1)
                        obj.profile.energy = sum(obj.profile.data.active)/3600;
                    else
                        obj.profile.energy = obj.profile.data.active*obj.profile.duration/3600;
                    end
                    obj.to_use=0;
                otherwise
                    % error catching
                    obj.ID = varargin{1};
                    obj.name = int2str(obj.ID);
            end 
        end
        function re = sum(obj)
            re = sum(obj.profile.data.active);
        end
    end
end

